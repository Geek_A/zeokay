// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const mkdirp = require('mkdirp-promise');
const gcs = require('@google-cloud/storage')();
const spawn = require('child-process-promise').spawn;
const LOCAL_TMP_FOLDER = '/tmp/';
const logging = require('@google-cloud/logging')();
const nodemailer = require('nodemailer');
const request = require('request-promise');
const EmailTemplates = require('swig-email-templates');
const Q = require('q');
const moment = require('moment');
const stripe = require('stripe')(functions.config().stripe.token);
const currency = functions.config().stripe.currency || 'USD';
const gmailEmail = encodeURIComponent(functions.config().gmail.email);
const gmailPassword = encodeURIComponent(functions.config().gmail.password);
const mailTransport = nodemailer.createTransport(`smtps://${gmailEmail}:${gmailPassword}@smtp.gmail.com`);

// Max height and width of the thumbnail in pixels.
const THUMB_MAX_HEIGHT = 200;
const THUMB_MAX_WIDTH = 200;
// Thumbnail prefix added to file names.
const THUMB_PREFIX = 'thumb_';


// Keeps track of the length of the 'likes' child list in a separate property.
// Keeps track of the length of the 'likes' child list in a separate property.
exports.countUserVideos = functions.database.ref('/videos/{videoid}/ownerUid').onWrite(event => {
  const userId = event.data.val();
  const uref = event.data.ref.parent.parent.parent.child('users/' + userId + '/subscription/remaining_proofs');
  uref.transaction(function (currentData) {
    if (currentData == null)
      return 0;
    else
      return Math.max(currentData - 1, 0);
  });
});

exports.countnoteschange = functions.database.ref('/notation/{nUid}').onWrite(event => {
  const noteRef = event.data.ref.parent;
  const userId = event.data.val().userUid;
  const userRef = noteRef.parent.child('users/' + userId + '/note');
  const uref = noteRef.orderByChild('userUid').startAt(userId).endAt(userId).once('value', function (snap) {

    var notes = snap.val() ? snap.val() : [];
    var somme = Object.keys(notes).map((key) => notes[key].note).reduce((a, b) => a += b);
    var note = Math.round(somme / Object.keys(notes).length);
    return userRef.transaction(current => {
      return note;
    }).then(() => {
      console.log('user note is: ', note);
    });
  });
});

exports.setVideoHasUpdates = functions.database.ref('/proof_info/{infoId}').onWrite(event => {
  const videoInfo = event.data.val();
  console.log(videoInfo);
  const uref = event.data.ref.parent.parent.child('videos/' + videoInfo.videoId);
  uref.once('value').then(function (dataSnapshot) {
    var cVideo = dataSnapshot.val();
    if (cVideo) {
      videoData = {
        owner_delete: 0,
        recieve_delete: 0,
        updatedAt: new Date().getTime()
      };

      videoData[videoInfo.type] = 1;

      if (cVideo.ownerUid === videoInfo.fromId)
        videoData['status'] = {
          hasOwnerUpdates: 0,
          hasReceiverUpdates: 1
        };
      else
        videoData['status'] = {
          hasOwnerUpdates: 1,
          hasReceiverUpdates: 0
        };

      return uref.update(videoData);
    }
  });
});

exports.setConversationHasUpdates = functions.database.ref('/messages/{msgId}').onWrite(event => {
  const message = event.data.val();
  console.log(message);
  const uref = event.data.ref.parent.parent.child('conversations/' + message.conversation);
  uref.once('value').then(function (dataSnapshot) {
    var conv = dataSnapshot.val();
    if (conv) {
      if (conv.fromId === message.fromId)
        return uref.update({
          owner_delete: 0,
          recieve_delete: 0,
          updatedAt: new Date().getTime(),
          status: {
            hasOwnerUpdates: 0,
            hasReceiverUpdates: 1
          }
        });
      else
        return uref.update({
          owner_delete: 0,
          recieve_delete: 0,
          updatedAt: new Date().getTime(),
          status: {
            hasOwnerUpdates: 1,
            hasReceiverUpdates: 0
          }
        });
    }
  });
});

// When a user is created, register them with Stripe
exports.createStripeCustomer = functions.auth.user().onCreate(event => {
  const data = event.data;
  return stripe.customers.create({
    email: data.email
  }).then(customer => {
    sendMessageAfterAction("Votre compte Stripe est crée avec succès, vous pouvez passer vos paiements.", data.uid, "text");
    return admin.database().ref(`/stripe_customers/${data.uid}/customer_id`).set(customer.id);
  }, error => {
    sendMessageAfterAction(error.message, event.params.userId, "text_error");
  });
});

//delete source
exports.deleteSource = functions.database.ref('/stripe_customers/{userId}/deletesource/{sourceId}/{keycard}').onWrite(event => {
  const data = event.data;

  return admin.database().ref(`/stripe_customers/${event.params.userId}/customer_id`).once('value').then(snapshot => {
    return snapshot.val();
  }).then(customer => {
    console.log(event.params.keycard);
    stripe.customers.deleteSource(customer, event.params.sourceId).then(response => {
      admin.database().ref(`/stripe_customers/${event.params.userId}/deletesource`).remove();
      admin.database().ref(`/stripe_customers/${event.params.userId}/sources/${event.params.keycard}`).remove();
      sendMessageAfterAction("Votre carte de paiement est supprimé avec succès.", event.params.userId, "text");
    }, error => {
      sendMessageAfterAction(error.message, event.params.userId, "text_error");
    });

  });
});

//delete subscriptions
exports.deleteSubscription = functions.database.ref('/stripe_customers/{userId}/deletesubscription/{pushId}').onWrite(event => {
  // Only edit data when it is first created.
  if (event.data.previous.exists()) {
    return;
  }
  // Exit when the data is deleted.
  if (!event.data.exists()) {
    return;
  }

  return admin.database().ref(`/stripe_customers/${event.params.userId}/customer_id`).once('value').then(snapshot => {
    return snapshot.val();
  }).then(customer => {
    return stripe.subscriptions.list({
        customer: customer
      }).then(response => {       

        for (let index in response.data) {
          element = response.data[index];          
          stripe.subscriptions.del(element.id);
        }
        stripe.subscriptions.create({
          customer: customer,
          plan: 'free'
        }).then(response => {

          admin.database().ref(`/users/${event.params.userId}/subscription`).update({
            remaining_photos: 1,
            formula_id: 'free',
            remaining_proofs: 3,
            updatedAt: new Date().getTime()
          });

          var context = {
            subject: 'Votre abonnement est résilié avec succès.',
            date: moment().format('l')
          };
          successNotificationAfterAction(event, context, 'unsubscribe');
        });
      }, error => {

        sendMessageAfterAction(error.message, event.params.userId, "text_error");
        console.log(error);
        // We want to capture errors and render them in a user-friendly way, while
        // still logging an exception with Stackdriver
        return event.data.adminRef.set({
          executedAt: new Date().getTime(),
          error: userFacingMessage(error)
        }).then(() => {
          return reportError(error, {
            user: event.params.userId
          });
        });

      });
  });
});

//update default source
exports.Source = functions.database.ref('/stripe_customers/{userId}/defaultsource/{sourceId}').onWrite(event => {

  const request = event.data.val();
  const userId = event.params.userId;

  // Only edit data when it is first created.
  if (event.data.previous.exists()) {
    return;
  }
  // Exit when the data is deleted.
  if (!event.data.exists()) {
    return;
  }

  return admin.database().ref(`/stripe_customers/${event.params.userId}/customer_id`).once('value').then(snapshot => {
    return snapshot.val();
  }).then(customer => {
    return stripe.customers.update(customer, {
      default_source: request.cardId
    });
  }).then(response => {
      admin.database().ref(`/users/${event.params.userId}`).update({
        default_source: request.cardId
      });
      
      var context = {
        subject: "La carte principale est mis à jour avec succès.",
        date: moment().format('l')
      };
    
      successNotificationAfterAction(event, context, null);
  }).catch((error) => {
    console.error(error);
    createTranslationPromise('EN', 'FR', error).then(message => {
      sendMessageAfterAction("Echec de votre demande de changement de carte par defaut ! " + message, event.params.userId, "text_error");
    });
  });
});

// Add a payment source (card) for a user by writing a stripe payment source token to Realtime database
exports.addPaymentSource = functions.database.ref('/stripe_customers/{userId}/sources/{pushId}').onWrite(event => {
  const request = event.data.val();
  const userId = event.params.userId;
  const source = request.token

  // Only edit data when it is first created.
  if (event.data.previous.exists()) {
    return;
  }
  // Exit when the data is deleted.
  if (!event.data.exists()) {
    return;
  }

  if (source === null) return null;
  return admin.database().ref(`/stripe_customers/${event.params.userId}/customer_id`).once('value').then(snapshot => {
    return snapshot.val();
  }).then(customer => {    
    return stripe.customers.createSource(customer, {
      source
    });
  }).then(response => {
    console.log(response);
    event.data.ref.update(response);

    var context = {
        subject: "Votre carte est crée avec succès.",
        date: moment().format('l')
      };
    successNotificationAfterAction(event, context, null);
  }).catch((error) => {
    console.error(error);
    createTranslationPromise('EN', 'FR', error).then(message => {
      sendMessageAfterAction("Echec de votre demande d'ajout de carte ! " + message, event.params.userId, "text_error");
    });
  });
});

// addSubscription
exports.addSubscription = functions.database.ref('/stripe_customers/{userId}/plan/{id}').onWrite(event => {
  const plan = event.data.val();
  const userId = event.params.userId;
  var customer;

  console.log('plan:' + plan);
  // Only edit data when it is first created.
  if (event.data.previous.exists()) {
    return;
  }
  // Exit when the data is deleted.
  if (!event.data.exists()) {
    return;
  }
  
  return admin.database().ref(`/stripe_customers/${event.params.userId}/customer_id`).once('value').then(snapshot => {
    customer = snapshot.val();
    if (!customer)
      return Promise.reject("L'utilisateur n'est pas correctement configurer");
    else
      return customer;
  }).then(customer => {
    // remove any exiting subscription for now
    // ToDO use update
    var promises = [];
    stripe.subscriptions.list({
      customer: customer
    }).then(response => {
      for (let index in response.data) {
        element = response.data[index];
        //catch the errors here to continue anyway
        promises.push(stripe.subscriptions.del(element.id).catch(error =>{console.error(error);}));
      }
    });

    return Promise.all(promises);
  }).then(() => {
    console.log('params:' + {customer: customer,plan: plan.plan});
    if (!customer || !plan)
      return Promise.reject("Le Plan demandé (" + plan + ") n'est pas valide!");
    else
      return stripe.subscriptions.create({
        customer: customer,
        plan: plan.plan
      });
  }).then(response => {
    console.log('stripe response :' + JSON.stringify(response));
    admin.database().ref(`/users/${event.params.userId}/subscription`).update({
      remaining_photos: plan.remaining_photos,
      formula_id: plan.formula_id,
      remaining_proofs: plan.remaining_proofs,
      subscribedAt: plan.subscribedAt,
      current_sub: response.id
    });

    var context = {
      formulat: response.plan.name,
      subject: "Votre abonnement à la formule (" + response.plan.name + ") est validée !",
      date: moment().format('l'),
      amount: response.plan.amount / 100 + ' ' + response.plan.currency
    };
    successNotificationAfterAction(event, context, 'subscribe');

  }).catch((error) => {
    console.error(error);
    createTranslationPromise('EN', 'FR', error).then(message => {
      sendMessageAfterAction("Votre demande d'abonnement n'est pas passé ! avce l'erreur: " + message, userId, "text_error");
    });
  });
});

// addSubscription
exports.addUnitBuy = functions.database.ref('/stripe_customers/{userId}/unitbuy/{id}').onWrite(event => {
  const plan = event.data.val();
  console.log(plan);
  if (plan === null) return null;
  return admin.database().ref(`/stripe_customers/${event.params.userId}/customer_id`).once('value').then(snapshot => {
    return snapshot.val();
  }).then(customer => {
    const amount = plan.amount;
    const idempotency_key = event.params.id;
    let charge = {
      amount,
      currency,
      customer
    };

    return stripe.charges.create(charge, {
      idempotency_key
    });
  }).then(response => {
    admin.database().ref(`/stripe_customers/${event.params.userId}/unitbuy`).remove();
    admin.database().ref(`/users/${event.params.userId}/subscription`).update({
      remaining_proofs: plan.remaining_proofs,
      updatedAt: plan.updatedAt
    });
    sendMessageAfterAction("Votre paiement unitaire est passé avec succès !", event.params.userId, "text");
  }).catch((error) => {
    return event.data.adminRef.parent.child('error').set(userFacingMessage(error)).then(() => {
      return reportError(error, {
        user: user
      });
    });
  });
});

// When a user deletes their account, clean up after them
exports.cleanupUser = functions.auth.user().onDelete(event => {
  return admin.database().ref(`/stripe_customers/${event.data.uid}`).once('value').then(snapshot => {
    return snapshot.val();
  }).then(customer => {
    return stripe.customers.del(customer);
  }).then(() => {
    return admin.database().ref(`/stripe_customers/${event.data.uid}`).remove();
  });
});

//update default source
exports.downloadProof = functions.database.ref('/stripe_customers/{userId}/download/{requestId}').onWrite(event => {
  const request = event.data.val();
  const userId = event.params.userId;
  const templates = new EmailTemplates({
    root: 'templates'
  });
  return admin.database().ref(`/stripe_customers/${event.params.userId}/customer_id`).once('value').then(snapshot => {
    return snapshot.val();
  }).then(customer => {
    const amount = request.amount; //plan.download_amount;

    const idempotency_key = event.params.id;
    let charge = {
      amount,
      currency,
      customer
    };

    if (amount === 0)
      return Promise.resolve("Succès");
    else
      return stripe.charges.create(charge, {
        idempotency_key
      });
  }).then(response => {

    const mailOptions = {
      from: '"L\'équipe ZeOkay." <noreply@zeokay.com>',
      to: request.emails
    };
    // The user just subscribed to our newsletter.
    var defer = Q.defer();
    admin.database().ref("videos/" + request.proof).once('value').then(snapshot => {

      var video = snapshot.val();
      // set download time and flag for delete
      admin.database().ref("videos/" + request.proof + "/downloadedAt").set(new Date().getTime());

      var ref = admin.database().ref("proof_info");
      ref.orderByChild('videoId').startAt(request.proof).endAt(request.proof).once('value').then(snapshot => {


        joinPaths(snapshot.val(), 'users', 'fromId', 'fromProfile', function (results) {
          var context = {
            'subject': 'Télechargement de la preuve: ' + video.title,
            'messages': formatProofHTML(results),
            'name': video.title,
            'emails': request.emails
          };

          templates.render('download.html', context, function (err, html, text) {
            if (err)
              defer.reject(err);
            else
              defer.resolve(html);
          });
        }, function (error) {
          defer.reject(error);
        });
      });

      return defer.promise;
    }).then(proof => {
      mailOptions.subject = 'Télechargement de la preuve ';
      mailOptions.html = proof;
      return mailTransport.sendMail(mailOptions).then(() => {
        console.log('New subscription confirmation email sent to:', request.emails);
        sendMessageAfterAction("Votre paiement pour télechargement est passé avec succès !", userId, "text");
      });
    });
  }).catch(function (error) {
    console.error(error);
    createTranslationPromise('EN', 'FR', error).then(message => {
      sendMessageAfterAction("Votre paiement pour télechargement n'est pas passé ! " + message, userId, "text_error");
    });
  });
});

exports.deleteArchiveVideo = functions.https.onRequest((req, res) => {

admin.database().ref('/videos').once('value').then(snapshot => {

 snapshot.foreach(function(videos){
 var element = videos.val();
      console.log(videos.val()); 
     // console.log(posts.key); // ID of the post
	  if (element.archiveAt && element.archiveAt < new Date().getTime()) {
	  console.log("deleeeete");
			//	admin.database().ref('/videos/${event.data.uid}').remove();
	}
  });
	});
   const hours = (new Date().getHours() % 12) + 1 // london is UTC + 1hr;
  res.status(200).send(`<!doctype html>
    <head>
      <title>Time</title>
    </head>
    <body>
      ${'BONG '.repeat(hours)}
    </body>
  </html>`);
});

exports.checkSubscription = functions.https.onRequest((req, res) => {
 stripe.subscriptions.list().then(response => {       
        for (let index in response.data) {
          element = response.data[index];
         // stripe.subscriptions.del(element.id);
        }
		});
});


function joinPaths(elements, path, key, alias, onSuccess, onFail) {

    if(!elements)
      return onSuccess({});

    var returnCount = 0;
    var expectedCount = Object.keys(elements).length;
    var ref = admin.database().ref();

    Object.keys(elements).map(function (p) {
        var id = elements[p][key];
        ref.child(path + '/' + id).once('value',
            // success
            function (snap) {
                // add it to the merged data
                elements[p][alias] = snap.val();

                // when all paths have resolved, we invoke
                // the callback (jQuery.when would be handy here)
                if (++returnCount === expectedCount) {
                    onSuccess(elements);
                }
            },
            // error
            function (error) {
                returnCount = expectedCount + 1; // abort counters
                onFail(error);
            }
        );
    });
}

/********************************
* Start Utis functons
*********************************/
function formatProofHTML(elements) {
  var html = "";  
  //sort messages
  var keysSorted = Object.keys(elements).sort(function (a, b) {
    return elements[a].updatedAt - elements[b].updatedAt;
  });

  keysSorted.map(function (key) {
    var message = elements[key];
    var fromUser = message.fromProfile ? message.fromProfile.firstname + ', ' + message.fromProfile.name : '';
    var sendAt = moment(message.updatedAt).format("h:mm:ss DD-MM-YYYY");

    if (message.type == 'video' || message.type == 'video_response') {
      html +='<li>Video de (' + fromUser + ') envoyée à ' + sendAt + ' <br/><img class="pic" src="' + message.thumb + '" /></br><a href="' + message.url + '">Télécharger la !</a></li>';
    } else if (message.type == 'confirm_send_colis') {
      html +='<li>Colis de (' + fromUser + ') envoyé à ' + sendAt + ', détail du colis: ' + message.colisInfo + '</li>';
    } else if (message.type == 'confirm_proof_recept') {
      html += '<li>La preuve de réception est confirmée par (' + fromUser + ') à ' + sendAt + '</li>';
    } else if (message.type == 'refuse_proof_recept') {
      html += '<li>La preuve de réception est déclinée par (' + fromUser + ') à ' + sendAt + '</li>';
    } else if (message.type == 'confirm_recept_colis') {
      html += '<li>Colis reçu par (' + fromUser + ') à ' + sendAt + ' !</li>';
    } else if (message.type == 'confirm_colis_recept') {
      html += '<li>Le colis est validé par (' + fromUser + ') à ' + sendAt + '</li>';
    } else if (message.type == 'refuse_colis_recept') {
      html += '<li>Le colis est décliné par (' + fromUser + ') à ' + sendAt + ', détails: ' + message.refuseInfo + ' </li>';
    } else if (message.type == 'validate') {
      html += '<li>La preuve est validée par (' + fromUser + ') à ' + sendAt + ' </li>';
    } else if (message.type == 'refuse') {
      html += '<li>La preuve est déclinée par (' + fromUser + ') à ' + sendAt + ', détails: ' + message.refuseInfo + '  </li>';
    } else if (message.type == 'notation' || message.type == 'notation_owner') {
      html += '<li>La notation de cette preuve par (' + fromUser + ') est ' + message.notation + ' / 5 à ' + sendAt + '<br /><b>Commentaire:</b> ' + message.notationInfo + '</li>';
    } else if (message.type == 'photo') {
      html += '<li>Image de (' + fromUser + ') à ' + sendAt + ' :<br/><img class="pic" src="' + message.url + '" /></li>';
    } else {
      html += '<li>Message de (' + fromUser + ') à ' + sendAt + ' :<b>' + message.message + '</b></li>';
    }
  });

  return html;
}


function successNotificationAfterAction(event, context, emailTemplate) {
  if(emailTemplate)
    sendMailAfterAction(event.params.userId, context.subject, emailTemplate, context);

  sendMessageAfterAction(context.subject, event.params.userId, "text");

  //update execution logs
  event.data.adminRef.update({
    executedAt: new Date().getTime(),
    message: context.subject
  });
}
//not finished
function sendMessageAfterAction(message, to, type) {
  admin.database().ref('/users/' + to).once('value').then(snapshot => {
    return snapshot.val();
  }).then(dataU => {
    var db = admin.database();
    var contact_admin = "";
    var adminId = "PExyCZk0vuOB1XdY69wNHI3x1up2";
    if (!dataU.contact_admin) {
      var ref = db.ref("conversations");

      var convData = {
        fromId: adminId,
        toId: to,
        createdAt: new Date().getTime(),
        updatedAt: new Date().getTime()
      };

      // var converRef = ref.child("conversations");
      var newConvRef = ref.push();
      var convKey = newConvRef.key;
      newConvRef.set(convData);
      admin.database().ref('/users/' + to).update({
        contact_admin: convKey
      });
      contact_admin = convKey;
    } else {
      contact_admin = dataU.contact_admin;
    }

    var msgData = {
      conversation: contact_admin,
      fromId: adminId,
      toId: to,
      updatedAt: new Date().getTime(),
      message: message,
      type: type
    };
    var ref = db.ref("messages");
    var newMsgKey = ref.push().key;
    admin.database().ref('messages/' + newMsgKey).set(msgData);
    admin.database().ref('/conversations/' + contact_admin).update({
      lastMessage: message
    });

    var notifData = {
        message: message,
        action: "message/" + contact_admin,
        fromUser: adminId,
        user: to,
        updatedAt: new Date().getTime(),
        read: 0        
    };
    admin.database().ref('/notifications').push(notifData);
  });
}

function sendMailAfterAction(userId, subject, template, context) {
  var templates = new EmailTemplates({
    root: 'templates'
  });

  admin.database().ref('/users/' + userId).once('value').then(snapshot => {
    var user = snapshot.val();
    templates.render(template + '.html', context, function (err, html, text) {
        const mailOptions = {
          from: '"L\'équipe ZeOkay." <noreply@zeokay.com>',
          to: user.email
        };
        if (!err) {
          mailOptions.subject = subject;
          mailOptions.html = html;
          return mailTransport.sendMail(mailOptions).then(() => {
            console.log('email sent to:', user.email);
          });
        }
      });
  });
}

// To keep on top of errors, we should raise a verbose error report with Stackdriver rather
// than simply relying on console.error. This will calculate users affected + send you email
// alerts, if you've opted into receiving them.
// [START reporterror]
function reportError(err, context = {}) {
  // This is the name of the StackDriver log stream that will receive the log
  // entry. This name can be any valid log stream name, but must contain "err"
  // in order for the error to be picked up by StackDriver Error Reporting.
  const logName = 'errors';
  const log = logging.log(logName);

  // https://cloud.google.com/logging/docs/api/ref_v2beta1/rest/v2beta1/MonitoredResource
  const metadata = {
    resource: {
      type: 'cloud_function',
      labels: {
        function_name: process.env.FUNCTION_NAME
      }
    }
  };

  // https://cloud.google.com/error-reporting/reference/rest/v1beta1/ErrorEvent
  const errorEvent = {
    message: err.stack,
    serviceContext: {
      service: process.env.FUNCTION_NAME,
      resourceType: 'cloud_function'
    },
    context: context
  };

  // Write the error log entry
  return new Promise((resolve, reject) => {
    log.write(log.entry(metadata, errorEvent), error => {
      if (error) {
        reject(error);
      }
      resolve();
    });
  });
}
// [END reporterror]

// Sanitize the error message for the user
function userFacingMessage(error) {
  var err = error.type ? error.message : 'An error occurred, developers have been alerted';
  createTranslationPromise('EN', 'FR', err).then(message => {
      return message;
  }).catch(() => {return err});
}

// URL to the Google Translate API.
function createTranslateUrl(source, target, payload) {
  return `https://www.googleapis.com/language/translate/v2?key=${functions.config().firebase.apiKey}&source=${source}&target=${target}&q=${payload}`;
}

function createTranslationPromise(source, target, message) {
  return request(createTranslateUrl(source, target, message), {
    resolveWithFullResponse: true
  }).then(
    response => {
      if (response.statusCode === 200) {
        const data = JSON.parse(response.body).data;
        return unescape(data.translations[0].translatedText);
      } else
        return message;      
    }).catch(() => {return message});
}

