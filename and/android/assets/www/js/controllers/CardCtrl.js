(function() {
    'use strict';
    zeokey.controller('CardCtrl', ['$scope', '$rootScope', '$location', '$ionicHistory', '$state', '$stateParams', '$window', '$ionicModal', '$timeout', '$interval', '$ionicPopup', '$cordovaNgCardIO', 'Utils', 'Popup', '$ionicListDelegate',
        function($scope, $rootScope, $location, $ionicHistory, $state, $stateParams, $window, $ionicModal, $timeout, $interval, $ionicPopup, $cordovaNgCardIO, Utils, Popup, $ionicListDelegate) {
            var sid;
            $scope.page = "";
            $scope.listCards = [];
            $scope.imgMenu = "img/icons/inbox-btn.png";
            $scope.imgMenuRed = "img/icons/inbox-btn.png";
            $scope.paiementshow = false;
            $scope.newcard = {};
            var stop;


            $scope.scanpay = function() {
                //console.log("scanpay");
                $cordovaNgCardIO.scanCard()
                        .then(function(response) {
                            $scope.newcard.cardnumber = response.card_number;
                            $scope.newcard.snumber = response.cvv;
                            $scope.newcard.expiredate = ('0' + response.expiry_month).slice(-2) + '/' + response.short_expiry_year;
                            $scope.$apply();
                        }, function(response) {
                        })
            };

            $scope.actionMenu = function() {
                $state.go('addpayment');
            };

            $scope.goTo = function(path) {
                $location.path(path);
            };
            
            $scope.submitNewCreditCard = function(card) {
                if (window.cordova && window.cordova.plugins && cordova.plugins.stripe) {
                    Utils.show();
                    // if ($scope.paymentForm.$valid) {
                    var expired = $scope.newcard.expiredate.split("/");
                    //console.log(expired);
                    cordova.plugins.stripe.createCardToken({
                        number: $scope.newcard.cardnumber,
                        cvc: $scope.newcard.snumber,
                        expMonth: expired[0],
                        expYear: expired[1],
                        name: $scope.newcard.ownername,
                        currency: 'EUR'
                    }, function(tokenId) {                      
                        firebase.database().ref('/stripe_customers/' + $rootScope.currentUser.uid + '/sources').push({
                            token: tokenId,
                            createdAt: firebase.database.ServerValue.TIMESTAMP
                        }).then(function() {
                            $scope.newcard.cardnumber = '';
                            $scope.newcard.snumber = '';
                            $scope.newcard.ownername = '';
                            Utils.hide();
                            Utils.message(Popup.successIcon, 'CARD_IN_PROGRESS'); 
                            $location.path('card');
                        }).catch(onFail);                        
                    }, onFail);


                    // } else {
                    //     Utils.message(Popup.errorIcon, "Erreur: Veuillez remplir tous les champs correctement");                     
                    // }
                } else {
                    onFail("Erreur: Veuillez contacter notre support");

                }

            };

            var onFail = function(error) {
                Utils.message(Popup.errorIcon, error);
            };

            $scope.selectDefaultCard = function(id) {
                //console.log(id);
                Utils.show();
                firebase.database().ref('/stripe_customers/' + $rootScope.currentUser.uid + '/defaultsource').push({
                    token: '',
                    cardId: id,
                    createdAt: firebase.database.ServerValue.TIMESTAMP
                }).then(function() {
                    $ionicListDelegate.closeOptionButtons();
                    Utils.message(Popup.successIcon, "OPERATION_IN_PROGRESS");                          
                }, function(error) {
                    Utils.message(Popup.errorIcon, "Opération non effectué (" + error + ")");
                });

            };

            $scope.deleteCard = function(id, key) {//console.log("deleteCard", key);
                //console.log(id);
                firebase.database().ref('/stripe_customers/' + $rootScope.currentUser.uid + '/deletesource/' + id + '/' + key).push({token: ''}).then(function() {
                    $ionicListDelegate.closeOptionButtons();
                    Utils.message(Popup.successIcon, "OPERATION_IN_PROGRESS");
                });
            };       

            $scope.submitNewCharge = function() {
                firebase.database().ref('/stripe_customers/' + $rootScope.currentUser.uid + '/charges').push({
                    source: this.newCharge.source,
                    amount: parseInt(this.newCharge.amount)
                });
            };

            $scope.priviousUrl = "setting";
            if ($ionicHistory.backView()) {
                $scope.priviousUrl = $ionicHistory.backView().url;
            }
        }
    ]);

})();