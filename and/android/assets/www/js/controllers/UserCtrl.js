(function() {
    'use strict';
    zeokey.controller('UserCtrl', ['$scope', '$rootScope', '$state', '$location', '$filter', '$cordovaCapture', '$ionicPopup', '$translate', '$timeout', '$stateParams', '$ionicHistory', '$sce', '$ionicPopover', '$ionicSlideBoxDelegate', '$ionicModal', 'Utils', 'Popup', 'PushServices',
        function($scope, $rootScope, $state, $location, $filter, $cordovaCapture, $ionicPopup, $translate, $timeout, $stateParams, $ionicHistory, $sce, $ionicPopover, $ionicSlideBoxDelegate, $ionicModal, Utils, Popup, PushServices) {
            $scope.nbrNotes = 0;
            $scope.isMyProfile = false;
            $scope.goTo = function(path, params) {
                $location.path(path);
            };
            $scope.close = function() {
                $scope.goTo('home');
                $scope.popover.hide();
            };


            // Called each time the slide changes
            $scope.slideChanged = function(index) {
                $scope.slideIndex = index;
            };
            $scope.priviousUrl = "home";
            if ($ionicHistory.backView()) {
                $scope.priviousUrl = $ionicHistory.backView().url;
            }
            $ionicPopover.fromTemplateUrl('templates/popover.html', {
                scope: $scope,
            }).then(function(popover) {
                $scope.popover = popover;
                var conv = firebase.database().ref("conversations");
                conv.on('value', function(data) {
                    var conversations = data.val();
                    $timeout(function() {
                        var listFriends = [];
                        for (var index in conversations) {
                            var attr = conversations[index];
                            var fId = "";
                            var find = false;
                            if (attr.fromId === $rootScope.currentUser.uid) {
                                fId = attr.toId;
                                find = true;
                            }
                            if (attr.toId === $rootScope.currentUser.uid) {
                                fId = attr.fromId;
                                find = true;
                            }
                            if (find) {
                                var item = {
                                    conversation: index,
                                    friendId: fId
                                };
                                listFriends.push(item);
                                ////console.log("list contact", listFriends);
                            }
                        }
                        $scope.isFriend = false;
                        for (var index in listFriends) {
                            if (listFriends[index].friendId == $stateParams.slug) {
                                $scope.isFriend = true;
                                $rootScope.currentFriend = listFriends[index];
                            }
                        }
                    });
                });

                $scope.saveNotification = function(message, userId) {
                    var database = firebase.database();
                    var newMsgKey = database.ref().child('notifications').push().key;
                    var notifData = {
                        message: message,
                        action: "user/" + $rootScope.currentUser.uid,
                        fromUser: $rootScope.currentUser.uid,
                        user: userId,
                        updatedAt: new Date().getTime(),
                        read: 0,
                        notificationKey: newMsgKey
                    };
                    
                    database.ref('notifications/' + newMsgKey).set(notifData);
                    database.ref('users/' + userId).on('value', function(profile) {
                        var user = profile.val();
                        if (user.deviceId) {
                            var promise = PushServices.sendPush(user.deviceId, message, "user/" + $rootScope.currentUser.uid);
                            promise.then(function(datas) {
                                //console.log(datas);
                            });
                        }
                    });
                };

                $scope.addFriend = function() {
                    $scope.popover.hide();
                    var confirmPopup = $ionicPopup.confirm({
                        title: $translate.instant("CONFIRM_ADD_FAVORITE_TITLE"),
                        template: $translate.instant("CONFIRM_ADD_FAVORITE_CONTENT")
                    });
                    confirmPopup.then(function(res) {
                        if (res) {
                            var convData = {
                                fromId: $rootScope.currentUser.uid,
                                toId: $stateParams.slug,
                                createdAt: new Date().getTime(),
                                updatedAt: new Date().getTime()
                            };
                            var database = firebase.database();
                            var newConvKey = firebase.database().ref().child('conversations').push().key;
                            database.ref('conversations/' + newConvKey).set(convData);
                            var message = "Vous avez reçu une nouvelle demande d'ajout en favoris.";
                            $scope.saveNotification(message, $stateParams.slug);
                            Utils.message(Popup.successIcon, $translate.instant("FAVORITE_SUCCESS_CONTENT"));
                        }
                    });
                };

                $scope.removeFriend = function() {
                    $scope.popover.hide();
                    //console.log($rootScope.currentFriend);
                    var confirmPopup = $ionicPopup.confirm({
                        title: $translate.instant("CONFIRM_DELETE_USER"),
                        template: $translate.instant("CONFIRM_DELETE_USER_CONTENT")
                    });
                    confirmPopup.then(function(res) {
                        if (res) {
                            var ref = firebase.database().ref("conversations/" + $rootScope.currentFriend.conversation);
                            ref.remove().then(function() {
                                Utils.message(Popup.successIcon, $translate.instant("FAVORITE_SUCCESS_REMOVE"));
                            });
                        }
                    });
                };

                $scope.sendMessage = function() {
                    $scope.popover.hide();
                    $location.path("/message/" + $rootScope.currentFriend.conversation);
                };
            });
            
            $scope.setProfil = function() {
                var userId = $stateParams.slug;
                //console.log(userId);
                $timeout(function() {
                    var ref = firebase.database().ref("videos");
                    ref.orderByChild("ownerUid").startAt(userId).endAt(userId).once("value").then(function(snapshot) {
                        $timeout(function() {
                            $scope.sendVideo = snapshot.numChildren();
                            $scope.valideProof = 0;
                            $scope.noValideProof = 0;
                            angular.forEach(snapshot.val(), function(f, key) {
                                if (f.validate === 1) {
                                    $scope.valideProof++;
                                } else {
                                    if (f.validate === 0) {
                                        $scope.noValideProof++;
                                    }
                                }
                            });
                            //console.log($scope.valideProof, $scope.noValideProof);
                        });
                    });
                    var ref = firebase.database().ref("videos");
                    ref.orderByChild("receiptUid").startAt(userId).endAt(userId).once("value").then(function(snapshot) {
                        $timeout(function() {
                            $scope.receiptVideo = snapshot.numChildren();
                        });
                    });
                    var ref = firebase.database().ref("notation");
                    ref.orderByChild("userUid").startAt(userId).endAt(userId).once("value").then(function(snapshot) {
                        $timeout(function() {
                            $scope.notations = [];
                            angular.forEach(snapshot.val(), function(f, key) {
                                firebase.database().ref('users/' + f.commUserUid).on('value', function(data) {
                                    f.profile = data.val();
                                });
                                $scope.notations.push(f);
                            });
                            //$scope.notations = snapshot.val();
                            $scope.nbrNotes = Object.keys($scope.notations).length;
                            $ionicSlideBoxDelegate.update();
                        });
                    });
                    firebase.database().ref('users/' + userId).on('value', function(profile) {
                        var profil = profile.val();
                        $scope.profil = profil;
                        if (profil.createdAt) {
                            var CurrentDate = new Date(profil.createdAt);
                            var curr_date = CurrentDate.getDate();
                            var curr_month = CurrentDate.getMonth() + 1;
                            var curr_year = CurrentDate.getFullYear();
                            $scope.createdDate = curr_date + "/" + curr_month + "/" + curr_year;
                        }

                        if (profil.email === $rootScope.currentUser.email)
                            $scope.isMyProfile = true;

                        $scope.email = profil.email;
                        $scope.name = profil.name;
                        $scope.phone = profil.phone;
                        $scope.address = profil.address;
                        $scope.profilPic = profil.profile_picture;
                        var profile_formula = getFormula($rootScope.formulas, profil.subscription.formula_id);
                        if (profile_formula) {
                            var nbr_profile_video = profile_formula.nbr_profile_video;
                            if (nbr_profile_video > 0) {
                                var ref = firebase.database().ref("videos");
                                ref.orderByChild('ownerUid').startAt(userId).endAt(userId).once("value").then(function(snapshot) {
                                    $timeout(function() {
                                        var childKey = snapshot.val();
                                        var pathReference = firebase.storage().ref('videos/');

                                        // var totalVideos = Object.values(childKey).length;
                                        ////console.log(totalVideos);
                                        $scope.lastVideosPages = [];
                                        var lastVideos = [];
                                        var i = 0;
                                        angular.forEach(childKey, function(video, key) {
                                            if (!video.isResponse && video.owner_delete !== 1 && (profil.selected_videos && profil.selected_videos.includes(key))) {
                                                i++;
                                                lastVideos.push({
                                                    url: pathReference.child(video.name).getDownloadURL(),
                                                    thumb: video.thumb,
                                                    title: video.title,
                                                    createdAt: video.createdAt
                                                });
                                                if (i % 3 == 0) {
                                                    $scope.lastVideosPages.push(lastVideos);
                                                    lastVideos = [];
                                                    //console.log($scope.lastVideosPages);
                                                }
                                            }
                                        });
                                        if (lastVideos.length > 0) {
                                            $scope.lastVideosPages.push(lastVideos);
                                            lastVideos = [];
                                        }
                                    });
                                });
                            }
                        }
                    });
                });
            };
            $scope.openVideo = function(url) {

                $scope.link = url;
                Utils.video(url);
            };
            $scope.openPicture = function(url) {
                Utils.image(url);
            };

            $scope.setProfil();
            $scope.showSetting = function() {
                $state.go("setting");
            };

            function getFormula(formulas, name) {
                for (var index in formulas) {
                    var formula = formulas[index];
                    if (formula.id === name) {
                        return formula;
                    }
                }
            }
        }
    ]);
})();