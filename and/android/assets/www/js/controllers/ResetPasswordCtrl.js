(function() {
    'use strict';
    zeokey.controller('ResetPasswordCtrl', ['$scope', '$ionicLoading', '$ionicPopup', '$location', '$translate', 'Utils', 'Popup',
     function($scope, $ionicLoading, $ionicPopup, $location, $translate, Utils, Popup) {
            $scope.showNavBar = true;
            

            /*
             * go to url
             */
            $scope.goTo = function(path) {
                $location.path(path);
            };
            $scope.forgetPassword = function(email, form) {
                if (form.$valid) {
                    Utils.show();
                    firebase.auth().sendPasswordResetEmail(email).then(function(user) {
                        //console.log(user);
                        Utils.message(Popup.successIcon, $translate.instant("RESET_PASSWORD_CONTENT"));
                        $location.path("login");
                    }, function(error) {
                        //console.log(error);
                        Utils.message(Popup.errorIcon, error.message);
                    });
                }
            };
        }
    ]);
})();
