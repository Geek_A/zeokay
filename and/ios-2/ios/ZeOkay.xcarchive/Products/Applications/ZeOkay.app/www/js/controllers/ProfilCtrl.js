(function() {
    'use strict';
    zeokey.controller('ProfilCtrl', ['$scope', '$rootScope', '$location', '$cordovaCapture', '$ionicPopup', '$ionicModal', '$translate', '$timeout', '$ionicHistory', '$ionicPopover', 'Utils', 'Popup',
        function($scope, $rootScope, $location, $cordovaCapture, $ionicPopup, $ionicModal, $translate, $timeout, $ionicHistory, $ionicPopover, Utils, Popup) {
            $scope.priviousUrl = "setting";
            if ($ionicHistory.backView()) {
                $scope.priviousUrl = $ionicHistory.backView().url;
            }

            $scope.forms = {}
            // An alert dialog
            //$scope.imgMenu = "img/icons/save-btn.png";
            //$scope.imgMenuRed = "img/icons/save-btn.png";

            $rootScope.isLogged = false;
            $scope.goTo = function(path) {
                $location.path(path);
            };

            $ionicModal.fromTemplateUrl('templates/selectVideos.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
            });

            // Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function() {
                $scope.modal.remove();
            });

            $scope.showVideo = false;
            $scope.showVideos = function() {
               $scope.modal.show();
               // $scope.showVideo = true;
            };

            $scope.close = function() {
                $scope.modal.hide();
            };

            $scope.openVideo = function(video) {
                var pathReference = firebase.storage().ref('videos/');
                pathReference.child(video.name).getDownloadURL().then(function(url) {
                    Utils.video(url);
                });
            };

            $scope.selected_proof = {};

            $scope.saveVideos = function() {

                var keys = [];
                for (var k in $scope.selected_proof) {
                    if ($scope.selected_proof[k])
                     keys.push(k);
                }
                   
                $scope.showVideo = false;
                var user = $rootScope.currentUser;
                var userId = user.uid;
                //console.log(keys);
                firebase.database().ref('users/' + userId).update({
                    selected_videos: keys.join()
                }).then(function() {
                     return Utils.message(Popup.successIcon, "Votre profil est mise à jour avec succès ! \
                        Selon votre abonnement, " + $rootScope.currentFormula.nbr_profile_video + " preuve(s) seront afficher sur votre profil.");
                }).then(function() {$scope.modal.hide();});
               
                
            };

           
            $ionicPopover.fromTemplateUrl('templates/popover.html', {
                scope: $scope,
            }).then(function(popover) {
                $scope.popover = popover;
                $scope.showProfil = function() {
                    $scope.popover.hide();
                    //firebase.auth().onAuthStateChanged(function (user) {
                    var user = $rootScope.currentUser;
                    if (user) {
                        $timeout(function() {
                            var userId = user.uid;
                            $location.path('user/' + userId);
                        });
                    }
                    // });

                };
            })

            $scope.savePicture = function(imageName) {
                // firebase.auth().onAuthStateChanged(function (user) {
                var user = $rootScope.currentUser;
                if (user) {
                    var userId = user.uid;
                    firebase.database().ref('users/' + userId).update({
                        profile_picture: imageName
                    }).then(Utils.hide());
                } else {
                    $location.path("/login");
                }
                //  });
            };

            $scope.takePicture = function() {
                var options = {limit: 1};
                var popup = Utils.confirm("ion-link", "{{ 'TAKEPHOTO_QUESTION' | translate }}", "ion-images", "ion-camera");
                popup.then(function(isCamera) {
                    var imageSource;
                    if (isCamera) {
                        imageSource = Camera.PictureSourceType.CAMERA;
                    } else {
                        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
                    }
                    //Show loading.
                    Utils.getProfilePicture(imageSource);
                });

            };

            //Broadcast from our Utils.getPicture function that tells us that the image selected has been uploaded.
            $rootScope.$on('profileImageUploaded', function(event, args) {
                //Proceed with sending of image message.
                //console.log(args.imageUrl);
                $scope.profilPic = args.imageUrl;
                $scope.savePicture(args.imageUrl);
                Utils.hide();
            });
            $scope.toObject = function(selected) {

                var rv = {};
                for (var i = 0; i < selected.length; ++i)
                    rv[selected[i]] = true;
                return rv;
            };
            $scope.setProfil = function() {
                //console.log("setProfil");
                
                //firebase.auth().onAuthStateChanged(function (user) {
                var user = $rootScope.currentUser;
                if (user) {
                    $timeout(function() {
                        var userId = user.uid;
                        firebase.database().ref('users/' + userId).on('value', function(profile) {
                            var profil = profile.val();
                            if (profil.selected_videos) {
                                 $scope.selected_proof = $scope.toObject(profil.selected_videos.split(','));
                            }
                            $scope.email = user.email;
                            $scope.username = profil.username;
                            $scope.password = "test1234";
                            $scope.name = profil.name;
                            $scope.firstname = profil.firstname;
                            $scope.phone = profil.phone;
                            $scope.address = profil.address;
                            $scope.profilPic = profil.profile_picture;
                            //$scope.$apply();
                        });
                    });
                }
                //});
            };
            $scope.setProfil();

            $scope.showSetting = function() {
                $location.path("/setting");
            };

            //Enlarge selected image when selected on view.
            $scope.enlargeImage = function(url) {
                Utils.image(url);
            };

            $scope.resetPassword = function(){
                $scope.popover.hide();
                firebase.auth().sendPasswordResetEmail($rootScope.currentProfile.email).then(function(user) {
                    //console.log(user);
                    Utils.message(Popup.successIcon, $translate.instant("RESET_PASSWORD_CONTENT"));
                }, function(error) {
                    //console.log(error);
                    Utils.message(Popup.errorIcon, error.message);
                });
            };

            $scope.myActionMenu = function() {
                //firebase.auth().onAuthStateChanged(function (user) {
                var form = $scope.forms.currentForm;
                var user = $rootScope.currentUser;
                if (user) {
                    var userId = user.uid;
                    // firebase.database().ref('users/' + userId).on('value', function(forme) {
                    //     var profil = profile.val();
                    //     $scope.name = profil.name;
                    //     $scope.firstname = profil.firstname;
                    //     $scope.phone = profil.phone;
                    //     $scope.address = profil.address;

                    // });
                    //console.log(profil);

                    firebase.database().ref('users/' + userId).update({
                        name: form.name.$viewValue,
                        firstname:form.firstname.$viewValue,
                        username: form.username.$viewValue,
                        phone: form.phone.$viewValue,
                        address: form.address.$viewValue ? form.address.$viewValue : ""
                    }).then(function(user) {
                        //console.log(user);
                        Utils.message(Popup.successIcon, $translate.instant("PROFIL_UPDATE_CONTENT"));
                    }, function(error) {
                        //console.log(error);
                        Utils.message(Popup.errorIcon, error.message);
                    });
                } else {
                    $location.path("/login");
                }
                //});
            };
        }
    ]);
})();
