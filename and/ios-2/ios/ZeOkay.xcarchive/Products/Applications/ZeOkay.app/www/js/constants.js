/***
 *  Définition des constantes globales utilisées dans toute l'application.
 *
 */

zeokey.constant('StripeConfig', {
        stripeAppId: "pk_test_uhAnvCCiYFpKFWFBLWe3u609"
    }).constant('Social', {
        facebookAppId: "394598800935869", // "1107628639357351",
        googleWebClientId: "625247332461-snmt6rcf944um9i97s24ksh39b2r5akm.apps.googleusercontent.com",
        twitterKey: "aJWByCgPhUgYZJMojyFeH2h8F",
        twitterSecret: "XxqKHi6Bq3MHWESBLm0an5ndLxPYQ2uzLtIDy6f9vgKKc9kemI"
    })
    .constant('FirebaseConfig', {       
        apiKey: "AIzaSyAV3rCFwgu7T-cKrbqXpYHGww8rdoKZWJ8",
        authDomain: "zeokey-b2fe6.firebaseapp.com",
        databaseURL: "https://zeokey-b2fe6.firebaseio.com",
        storageBucket: "zeokey-b2fe6.appspot.com",
        messagingSenderId: "625247332461"
    }).constant('Popup', {
        delay: 5000, //How long the popup message should show before disappearing (in milliseconds -> 3000 = 3 seconds).
        successIcon: "ion-ios-checkmark-outline balanced",
        errorIcon: "ion-ios-close-outline energized",
        accountCreateSuccess: "Votre compte a été créé avec succès",
        emailAlreadyExists:   "Désolé, cette adresse email existe déjà dans notre base de données. Choisissez un autre email et réessayez.",
        usernameAlreadyExists:   "Désolé, ce nom d'utilisateur existe déjà. Choisissez un nom d'utilisateur différent et réessayez.",
        accountAlreadyExists:   "Désolé, mais un compte avec les mêmes informations d'identification existe déjà. Veuillez vérifier votre compte et réessayer.",
        emailNotFound:   "Désolé, une erreur est survenue. Veuillez vérifier votre compte et réessayer.",
        userNotFound:   "Désolé, une erreur est survenue. Veuillez vérifier votre compte et réessayer.",
        invalidEmail:   "Désolé, courrier électronique non valide. Veuillez vérifier votre email et réessayer.",
        notAllowed:   "Désolé, mais l'enregistrement est actuellement désactivé. Veuillez contacter le support et réessayer.",
        serviceDisabled:   "Désolé, mais la connexion à ce service est actuellement désactivée. Veuillez contacter le support et réessayer.",
        wrongPassword:   "Désolé, identifiant ou mot de passe invalide. Vérifiez votre compte et réessayez.",
        accountDisabled:   "Désolé, mais votre compte a été désactivé. Veuillez contacter le support et réessayer.",
        weakPassword:   "Désolé, mais vous avez entré un mot de passe faible. Veuillez entrer un mot de passe plus fort et réessayer.",
        errorRegister:   "Désolé, une erreur s'est produite. Veuillez réessayer ultérieurement.",
        passwordReset:   "Un lien de réinitialisation de mot de passe a été envoyé à : ",
        errorPasswordReset:   "Désolé, une erreur s'est produite. Veuillez réessayer ultérieurement.",
        errorLogout:   "Désolé, mais nous avons rencontré une erreur pour vous annuler. Essayez plus tard.",
        sessionExpired:   "Désolé, votre session a expiré. Essayez d'ouvrir une session à nouveau.",
        errorLogin:   "Désolé, mais nous avons rencontré une erreur. Veuillez réessayer plus tard.",
        welcomeBack:   "Nous saluons le retour!",
        manyRequests:   "Désolé, mais nous essayons encore votre connexion précédente. Essayez plus tard.",
        uploadImageError:   "Désolé, mais nous avons rencontré une erreur de téléchargement de votre image. Veuillez réessayer plus tard."
    });