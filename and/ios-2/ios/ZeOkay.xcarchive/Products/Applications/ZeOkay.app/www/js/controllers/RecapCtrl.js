(function() {
    'use strict';
    zeokey.controller('RecapCtrl', ['$scope', '$rootScope', '$ionicPopup', '$location', 'Popup', 'Utils',
        function($scope, $rootScope, $ionicPopup, $location, Popup, Utils) {
            $scope.goTo = function(path) {
                $location.path(path);
            };
            $scope.doPaiement = function() {
                //console.log($rootScope.sources);

                if (!$rootScope.sources || $rootScope.sources.length === 0) {
                    var confirmPopup = Utils.confirmtxt("ion-information-circled",
                            "POPUP_NO_CARD",
                            "SAVE", "CANCEL");
                    confirmPopup.then(function(res) {
                        if (res) {
                            $location.path("card");
                        }
                    });
                } else {
                    if ($rootScope.currentFormula.price.month > $rootScope.selectedFormula.price.month) {
                        Utils.message(Popup.errorIcon, 'POPUP_FORMULA_CHANGE_ERR'); 
                    } else {
                        //firebase.auth().onAuthStateChanged(function (user) {
                        var user = $rootScope.currentUser;
                        if (user) {
                            Utils.show();
                            //console.log($rootScope.selectedFormula);
                            firebase.database().ref('/stripe_customers/' + user.uid + '/plan').push({
                                plan: $rootScope.selectedFormula.id,
                                remaining_photos: $rootScope.selectedFormula.nbr_photos,
                                formula_id: $rootScope.selectedFormula.id,
                                remaining_proofs: $rootScope.selectedFormula.nbr_proof,
                                subscribedAt: new Date().getTime()
                            }).then(function() {
                                Utils.message(Popup.successIcon, 'PAYMENT_IN_PROGRESS'); 
                                $location.path("home");
                            }).catch(function(error){
                                Utils.message(Popup.errorIcon, error);
                            });                            
                        }
                    }
                }

            };


            var CurrentDate = new Date();
            CurrentDate.setMonth(CurrentDate.getMonth() + 1);
            var curr_date = CurrentDate.getDate();
            var curr_month = CurrentDate.getMonth() + 1;
            var curr_year = CurrentDate.getFullYear();
            $scope.dateEnd = curr_date + "." + curr_month + "." + curr_year;
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    // The index order is gotten from formulas slider
                    $rootScope.selectedFormula = $rootScope.formulas[$rootScope.currentIndex];
                    $rootScope.amountToPay = $scope.selectedFormula.price.month;
                }
            });
            // 

        }
    ]);
})();