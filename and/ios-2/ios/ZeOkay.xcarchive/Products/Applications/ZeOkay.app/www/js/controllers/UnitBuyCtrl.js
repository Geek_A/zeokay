(function() {
    'use strict';
    zeokey.controller('UnitBuyCtrl', ['$scope', '$rootScope', '$ionicPopup', '$location','Utils', 'Popup',
        function($scope, $rootScope, $ionicPopup, $location, Utils, Popup) {
            $scope.prrof_qty = 3;
            $scope.goTo = function(path) {
                $location.path(path);
            };
            $scope.doPaiement = function() {
                if (!$rootScope.sources || $rootScope.sources.length === 0) {
                    var confirmPopup = Utils.confirmtxt("ion-information-circled",
                            "POPUP_NO_CARD",
                            "SAVE", "CANCEL");
                    confirmPopup.then(function(res) {
                        if (res) {
                            $location.path("card");
                        }
                    });
                } else {
                    //firebase.auth().onAuthStateChanged(function (user) {
                    var user = $rootScope.currentUser;
                    if (user) {
                        //console.log($rootScope.selectedFormula);
                        firebase.database().ref('/stripe_customers/' + user.uid + '/unitbuy').push({
                            amount: $scope.calculAmount() * 100,
                            remaining_proofs: $scope.prrof_qty,
                            updatedAt: new Date().getTime()
                        }).then(function(){
                            Utils.message(Popup.successIcon, 'PAYMENT_IN_PROGRESS'); 
                        }).then(function(){
                            $location.path("home");
                        });
                    }
                }
            };

            //console.log($rootScope.currentIndex);
            var CurrentDate = new Date();
            CurrentDate.setMonth(CurrentDate.getMonth() + 1);
            //console.log(CurrentDate);
            var curr_date = CurrentDate.getDate();
            var curr_month = CurrentDate.getMonth() + 1;
            var curr_year = CurrentDate.getFullYear();
            $scope.dateEnd = curr_date + "." + curr_month + "." + curr_year;

            $scope.plusOne = function() {
                $scope.prrof_qty = Math.min($scope.prrof_qty + 1, 10);
            };
            $scope.minusOne = function() {
                $scope.prrof_qty = Math.max($scope.prrof_qty - 1, 3);
            };
            $scope.calculAmount = function() {
                return $scope.prrof_qty <= 3 ? 1.99 : $scope.prrof_qty * 0.5;
            }



        }
    ]);
})();
