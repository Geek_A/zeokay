﻿(function() {
    'use strict';
    zeokey.controller('ProofDetailCtrl', ['$scope', '$rootScope', '$location', '$stateParams', '$translate', '$ionicPopup', '$ionicScrollDelegate', '$window', '$sce', 'PushServices', '$cordovaCapture', '$ionicLoading', '$timeout', '$ionicModal', '$ionicPopover', '$templateCache', 'Utils', 'Popup',
        function($scope, $rootScope, $location, $stateParams, $translate, $ionicPopup, $ionicScrollDelegate, $window, $sce, PushServices, $cordovaCapture, $ionicLoading, $timeout, $ionicModal, $ionicPopover, $templateCache, Utils, Popup) {
            $scope.imgMenu = "img/icons/send-btn.png";
            $scope.imgMenuRed = "img/icons/send-btn.png";
            var firstThumbImage = new Image();
            firstThumbImage.src = "img/proof_covert.png";
            $scope.countPicture = 0;

            $timeout(function() {
                $scope.loaded = true;
            }, 3000);

            $scope.saveNotification = function(message, userId) {
                var database = firebase.database();
                var newMsgKey = database.ref().child('notifications').push().key;
                var notifData = {
                    message: message,
                    action: "proofdetail/" + $stateParams.slug,
                    fromUser: $rootScope.currentUser.uid,
                    user: userId,
                    updatedAt: firebase.database.ServerValue.TIMESTAMP,
                    read: 0,
                    notificationKey: newMsgKey
                };
                database.ref('notifications/' + newMsgKey).set(notifData);
                firebase.database().ref('users/' + userId).once('value').then(function(profile) {
                    var user = profile.val();
                    if (user && user.deviceId) {
                        var promise = PushServices.sendPush(user.deviceId, message, $location.path());
                        promise.then(function(datas) {
                            //console.log(datas);
                        });
                    }
                });
            };

            $scope.options = {
                content: '',
                size: 'big',
                /*background: '#0a73d2',
                 button: {background: 'rgba(255, 255, 255, 0)', cssClass: 'zbutton', size: 'big'},*/
                background: 'rgba(255, 255, 255, 0)',
                button: { background: 'rgba(255, 255, 255, 0)', size: 'big' },
                isOpen: false,
                toggleOnClick: true,
                items: [{
                        content: '',
                        cssClass: 'icon ion-camera zmenublue',
                        onclick: function() {
                            $scope.takePicture();
                        }
                    },
                    {
                        content: '',
                        cssClass: 'icon ion-android-drafts zmenublue',
                        onclick: function() {
                            $scope.prepareMessage();
                        }
                    },
                    {
                        content: '',
                        cssClass: 'icon ion-film-marker zmenublue',
                        onclick: function() {
                            $scope.reply();
                        }
                    }
                ]
            };

            $scope.hScroll = function(element) {
                var srcElement = event.srcElement;
                var element = event.srcElement.parentNode.childNodes[1];
                ionic.requestAnimationFrame(function() {
                    if ($ionicScrollDelegate.getScrollPosition()) {
                        if ($ionicScrollDelegate.getScrollPosition().top > 120) {
                            element.classList.remove("white");
                            srcElement.style.backgroundSize = '0%';
                        } else {
                            element.classList.add("white");
                            srcElement.style.backgroundSize = '100%';
                        }
                        if ($ionicScrollDelegate.getScrollPosition().top < 0) {
                            var sheight = 185 - $ionicScrollDelegate.getScrollPosition().top;
                            //(original height / original width) x new width = new height
                            var imgRealsize = (firstThumbImage.height / firstThumbImage.width) * window.innerWidth;
                            if (imgRealsize < sheight) {
                                srcElement.style.backgroundSize = 'auto ' + sheight + 'px';
                            } else
                                srcElement.style.backgroundSize = '100%';
                        }
                    }
                });
            };

            $scope.getMinHeight = function() {
                return { 'min-height': window.innerHeight - 40 + 'px' };
            };

            $scope.actionMenu = function() {
                $scope.options.isOpen = !$scope.options.isOpen;
            };

            $scope.getVideos = function() {
                $scope.firstVideo = "";
                $scope.secondVideo = "";
                //firebase.auth().onAuthStateChanged(function (user) {
                var user = $rootScope.currentUser;

                if (user) {
                    var videoId = $stateParams.slug;
                    firebase.database().ref('videos/' + videoId).once('value').then(function(data) {
                        var video = data.val();
                        video.key = data.key;
                        $scope.firstVid = video;
                        // do it here as it's faster
                        $scope.setFirstThumb(video.thumb);
                        //console.log($scope.firstVid);
                        $scope.showNotationMenu = false;
                        if (video.ownerUid != $rootScope.currentUser.uid) {
                            $scope.showNotationMenu = true;
                        }
                        $scope.ownerProfile = $rootScope.listUsers[video.ownerUid];
                        $scope.receiptProfile = $rootScope.listUsers[video.receiptUid];                   
                        $scope.avatarProfile = isOwner() ? $scope.receiptProfile : $scope.ownerProfile;                   
                        $scope.sendUser = video.owner_name;
                        $scope.infoVideo = video;
                    }).then(function() {
                        // update notifications status
                        if ($rootScope.currentUid === $scope.infoVideo.ownerUid)
                            firebase.database().ref('videos/' + $stateParams.slug + '/status').update({ hasOwnerUpdates: 0 });
                        else
                            firebase.database().ref('videos/' + $stateParams.slug + '/status').update({ hasReceiverUpdates: 0 });

                        angular.forEach($rootScope.notifications, function(notif, key) {
                            if(notif.user && notif.action && notif.user === user.uid && '/' + notif.action ===  $location.path())
                                firebase.database().ref('notifications/' + notif.key + '/read').set(1);
                        });

                    });
                }
                //  });
            };

	        $scope.getVideoInfos = function () {
	            var ref = firebase.database().ref("proof_info");
	            ref.orderByChild('videoId').startAt($stateParams.slug).endAt($stateParams.slug).on('value', function (snapshot) {
	                var elements = snapshot.val();
	                $timeout(function () {
	                    var keysSorted = Object.keys(elements).sort(function (a, b) {
	                        return elements[a].updatedAt - elements[b].updatedAt;
	                    });
	                    var values = keysSorted.map(function (key) {
	                        var value = elements[key];
	                        value.profile = $rootScope.listUsers[value.fromId];
	                        return value;
	                    });
	                    $scope.proofInfos = values;
	                    var dataLength = (values.length - 1);
	                    angular.forEach($scope.proofInfos, function (proof, key) {
	                        switch (proof.type) {
	                        case 'video':
	                           // $scope.setFirstThumb(proof.thumb);
	                            $scope.setFirstVideo(proof.url);
	                            break;
	                        case 'video_response':
	                            $scope.setResponseVideo();
	                            $scope.setSecondVideo(proof.url, proof);
	                            break;
	                        case 'confirm_send_colis':
	                            $scope.setConfirmSendColis();
	                            break;
	                        case 'confirm_proof_recept':
	                            $scope.setConfirmProofRecept();
	                            break;
	                        case 'refuse_proof_recept':
	                            $scope.setConfirmProofRecept();
	                            break;
	                        case 'confirm_recept_colis':
	                            $scope.setConfirmReceptColis();
	                            break;
	                        case 'validate':
	                            $scope.setHasAcceptStatus();
	                            break;
	                        case 'refuse':
	                            $scope.setHasRefuseStatus();
	                            break;
	                        case 'notation':
	                            $scope.setHasNotation();
	                            break;
	                        case 'notation_owner':
	                            $scope.setHasOwnerNotation();
	                            break;
	                        case 'photo':
	                            $scope.countPicture++;
	                            break;
	                        }

	                        if (dataLength == key) {
	                            if ($scope.loaded)
	                                $scope.scrollBottom();
	                            $scope.loaded = true;
	                        }

	                    });

	                });
	            });
	        };

            function makeid() {
                var text = '';
                var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                for (var i = 0; i < 5; i++) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return text;
            };

            $scope.saveVideo = function(url) {
                //console.log(url);
                // firebase.auth().onAuthStateChanged(function (userC) {
                var userC = $rootScope.currentUser;
                //console.log($scope.thumbnailUrl);
                var msgData = {
                    videoId: $stateParams.slug,
                    thumb: $scope.thumbnailUrl,
                    type: "video_response",
                    url: url,
                    fromId: userC.uid,
                    updatedAt: firebase.database.ServerValue.TIMESTAMP
                };

                var database = firebase.database();
                var newMsgKey = database.ref().child('proof_info').push().key;
                database.ref('proof_info/' + newMsgKey).set(msgData);
                //notif
                var message = "Vous avez reçu une réponse à votre preuve : " + $scope.firstVid.title + " de la part de " + $scope.currentProfile.name;
                $scope.saveNotification(message, $scope.firstVid.ownerUid);

                $ionicPopup.alert({
                    clickOutsideToClose: true,
                    title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                    cssClass: "popup_1",
                    okText: "Fermer",
                    template: '<p class="center">Votre vidéo est envoyée avec succès !</p>'
                });
                // });
                /* var videoData = {
                 name: name,
                 isResponse: true,
                 createdAt: firebase.database.ServerValue.TIMESTAMP,
                 send_user: $rootScope.currentProfile.email,
                 owner_name: $rootScope.currentProfile.name
                 };
                 
                 var database = firebase.database();
                 var newVideoKey = firebase.database().ref().child('videos').push().key;
                 database.ref('videos/' + newVideoKey).set(videoData);
                 firebase.database().ref('videos/' + $scope.infoVideo.key).update({
                 responseVideo: newVideoKey
                 });*/


            };

            var uploadVideoToFirebase = function(video) {

                Utils.generateVideoTumbnail(video).then(function(url) {
                    $scope.thumbnailUrl = url;
                }, onErrorReadFile);

                $window.resolveLocalFileSystemURL(video.localURL, function(fileEntry) {
                    fileEntry.file(function(file) {
                        var reader = new FileReader();
                        reader.onloadend = function() {
                            //console.log("Successful file write: " + this.result);
                            Utils.progress($scope);
                            var metadata = {
                                contentType: 'video/mp4',
                                maxOperationRetryTime: 100000,
                                maxUploadRetryTime: 100000
                            };

                            var blob = new Blob([new Uint8Array(this.result)], metadata);
                            var uploadTask = firebase.storage().ref().child('videos/' + video.name).put(blob);

                            if (uploadTask) {
                                uploadTask.on('state_changed', function(snapshot) {
                                    // Observe state change events such as progress, pause, and resume
                                    // See below for more detail
                                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                                    $scope.progressval = progress;
                                    //console.log('Upload is ' + progress + '% done');
                                    //$scope.$apply();

                                }, onErrorReadFile, function() {
                                    // Handle successful uploads on complete
                                    var downloadURL = uploadTask.snapshot.downloadURL;
                                    Utils.hide();
                                    $scope.saveVideo(downloadURL);
                                });
                            } else {
                                Utils.hide();
                            }
                        };
                        reader.readAsArrayBuffer(file);

                    }, onErrorReadFile);
                });
            };

            var onErrorReadFile = function(error) {
                Utils.message(Popup.errorIcon, "Error uploading: " + error);
            }

            $scope.simultane = function() {
                $timeout(function() {
                    if ($scope.firstVideo && $scope.secondVideo) {
                        Utils.videoSimu($scope.firstVideo, $scope.secondVideo);
                    } else {
                        $ionicPopup.alert({
                            title: "",
                            template: "<p class='center'>Il faut avoir deux vidéos pour lancer la lecture simultanée !</p>"
                        });
                    }
                }, 500);
            };

            $scope.reply = function(video) {
                $scope.simultane();
            };

            $scope.takeVideo = function() {
                var confirmPopup =  Utils.confirmtxt("ion-information-circled", "POPUP_REPLY", "OK_VIDEO_1", "CANCEL");
                confirmPopup.then(function(res) {
                    if (res) {
                        var duration = $rootScope.currentFormula.capture_duration ? 60 * $rootScope.currentFormula.capture_duration : 60;
                        var options = { limit: 1, duration: duration };
                        $cordovaCapture.captureVideo(options).then(function(videoData) {
                            $scope.secondVideo = videoData[0];
                            uploadVideoToFirebase(videoData[0]);
                        });
                    }
                });
            };

            $scope.videoRecept = function() {
                $scope.takeVideo();
            };
            $scope.firstThumb = "";
            $scope.setFirstThumb = function(url) {
                firstThumbImage.src = url;
                firstThumbImage.onload = function() {
                    $scope.firstThumb = url;
                };
            };
            $scope.hasResponseVideo = false;
            $scope.setResponseVideo = function() {
                $scope.hasResponseVideo = true;
            };
            $scope.firstVideo = "";
            $scope.setFirstVideo = function(url) {
                $scope.firstVideo = url;
            };

            $scope.secondVideo = "";
            $scope.setSecondVideo = function(url, proof) {
                $scope.secondVideo = url;
                $scope.secondVid = proof;
            };

            $scope.hasAcceptStatus = false;
            $scope.setHasAcceptStatus = function() {
                $scope.hasAcceptStatus = true;
            };
            $scope.hasRefuseStatus = false;
            $scope.setHasRefuseStatus = function() {
                $scope.hasRefuseStatus = true;
            };
            $scope.hasNotation = false;
            $scope.setHasNotation = function() {
                $scope.hasNotation = true;
            };
            $scope.hasOwnerNotation = false;
            $scope.setHasOwnerNotation = function() {
                $scope.hasOwnerNotation = true;
            };

            $scope.confirmSendColis = false;
            $scope.setConfirmSendColis = function() {
                $scope.confirmSendColis = true;
            };

            $scope.confirmReceptColis = false;
            $scope.setConfirmReceptColis = function() {
                $scope.confirmReceptColis = true;
            };
            $scope.confirmProofRecep = false;
            $scope.setConfirmProofRecept = function() {
                $scope.confirmProofRecep = true;
            };
           

            $ionicPopover.fromTemplateUrl('templates/popover.html', {
                scope: $scope,
            }).then(function(popover) {
                $scope.popover = popover;
                $scope.close = function() {
                    $scope.popover.hide();
                    $scope.goTo("home");
                };
                $scope.goToDownload = function() {
                    $scope.popover.hide();
                    $scope.goTo("download/" + $stateParams.slug);
                };
                $scope.setNotation = function(note) {
                    $scope.data.note = note;
                    var list = [].slice.call(event.srcElement.parentElement.children);
                    list.map(function(element) {
                        if(element.name && element.name <= note)
                            element.style.color = 'orange';
                        else
                            element.style.color = '#aaa';
                    });
                };
                
                $scope.showNotation = function() {
                    $scope.data = {valid: true};
                    //check if we are the owner or not
                    var toUserId = isOwner() ? $scope.receiptProfile.key : $scope.ownerProfile.key;
                    var typeNote = isOwner() ? 'notation_owner' : 'notation';
                    $scope.popover.hide();
                    $ionicPopup.show({
                        scope: $scope,
                        cssClass: "notation-dialog",
                        template: '<p style="text-align: center; font-size: 16px">Qu\'avez vous pensé de cette prestation?</p>' +
                                    '<div class="rating">' +
                                    '    <a href="" name="5" title="5 étoiles" ng-click="setNotation(5)">☆</a>' +
                                    '    <a href="" name="4" title="4 étoiles" ng-click="setNotation(4)">☆</a>' +
                                    '    <a href="" name="3" title="3 étoiles" ng-click="setNotation(3)">☆</a>' +
                                    '    <a href="" name="2" title="2 étoiles" ng-click="setNotation(2)">☆</a>' +
                                    '    <a href="" name="1" title="1 étoile" ng-click="setNotation(1)">☆</a>' +
                                    '</div>' +
                                    '<br />' +
                                    '<textarea ng-model="data.comm" rows="4" placeholder="Merci de laisser un commentaire"></textarea>'+
                                    '<span ng-if="!data.valid" style="text-align:center;font-size:12px" class="assertive">Merci de laisser un commentaire et une note</span>',
                        buttons: [{
                            text: $translate.instant("CANCEL"),
                            type: 'button-assertive',
                            onTap: function(e) {
                                return false;
                            }
                        },
                        {
                            text: $translate.instant("OK"),
                            type: 'button-positive',
                            onTap: function(e) {
                                
                                if(!$scope.data.comm || !$scope.data.note) {
                                    $scope.data.valid = false;
                                    event.preventDefault();
                                } else {
                                    var msgData = {
                                        videoId: $stateParams.slug,
                                        notation: $scope.data.comm,
                                        note: $scope.data.note,
                                        userUid: toUserId,
                                        commUserUid: $rootScope.currentUser.uid,
                                        updatedAt: firebase.database.ServerValue.TIMESTAMP
                                    };                                
                                    firebase.database().ref().child('notation').push(msgData);

                                    var msgData = {
                                        videoId: $stateParams.slug,
                                        type: typeNote, //"notation",
                                        notation: $scope.data.note,
                                        notationInfo: $scope.data.comm,
                                        fromId: $rootScope.currentUser.uid,
                                        updatedAt: firebase.database.ServerValue.TIMESTAMP
                                    };
                                    firebase.database().ref().child('proof_info').push(msgData);   

                                    var message = "Vous avez reçu un commentaire pour votre prestation au sujet de la preuve: " + $scope.firstVid.title;
                                    $scope.saveNotification(message, toUserId); 
                                }                        
                            }
                        }]
                    }, $scope);
                };            
            });

            $scope.confirmeSendColis = function() {
                $scope.data = {};
                var confirmPopup2 = $ionicPopup.confirm({
                    clickOutsideToClose: true,
                    scope: $scope,
                    title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                    cssClass: "popup_1",
                    okText: "Confirmer",
                    cancelText: "Annuler",
                    template: '<p class="center"><b>Merci d\'enregistrer les informations d\'envoi de colis. Ex : N° de suivi du colis... </b></p>\n\
                               <textarea name="note_comment" ng-model="data.comment" rows="4" placeholder="Merci de laisser un commentaire"></textarea>'
                }, $scope);

                confirmPopup2.then(function(res) {
                    if (res) {
                        var comment = "Aucun numéro pour ce colis a été enregistré";
                        if ($scope.data.comment) {
                            comment = $scope.data.comment;
                        }
                        var msgData = {
                            videoId: $stateParams.slug,
                            type: "confirm_send_colis",
                            colisInfo: comment,
                            fromId: $rootScope.currentUser.uid,
                            updatedAt: firebase.database.ServerValue.TIMESTAMP
                        };
                        var database = firebase.database();
                        var newMsgKey = firebase.database().ref().child('proof_info').push().key;
                        database.ref('proof_info/' + newMsgKey).set(msgData);

                        var message = "Le colis de la preuve : " + $scope.firstVid.title + " est envoyée.";
                        $scope.saveNotification(message, $scope.firstVid.receiptUid);

                        $ionicPopup.alert({
                            clickOutsideToClose: true,
                            title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                            cssClass: "popup_1",
                            okText: "Fermer",
                            template: '<p class="center">Merci d\'avoir confirmé l\'envoi du colis. <br />Pour plus de sécurité, vous pouvez envoyer une photo du bon de suivi de ce dernier.</p>'
                        });
                    }
                });
            };

            $scope.confirmeReceptColis = function() {
                var confirmPopup2 = $ionicPopup.confirm({
                    clickOutsideToClose: true,
                    title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                    cssClass: "popup_1",
                    okText: "Confirmer",
                    cancelText: "Annuler",
                    template: '<p class="center"><b>Avez-vous reçu le colis ?</b></p>'
                });

                confirmPopup2.then(function(res) {
                    if (res) {
                        var msgData = {
                            videoId: $stateParams.slug,
                            type: "confirm_recept_colis",
                            fromId: $rootScope.currentUser.uid,
                            updatedAt: firebase.database.ServerValue.TIMESTAMP
                        };
                        var database = firebase.database();
                        var newMsgKey = firebase.database().ref().child('proof_info').push().key;
                        database.ref('proof_info/' + newMsgKey).set(msgData);
                        var message = "Le colis de la preuve " + $scope.firstVid.title + " est reçu par " + $scope.firstVid.receipt_name;
                        $scope.saveNotification(message, $scope.firstVid.ownerUid);
                        $ionicPopup.alert({
                            clickOutsideToClose: true,
                            title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                            cssClass: "popup_1",
                            okText: "Fermer",
                            template: '<p class="center">Merci d\'avoir confirmé la réception de colis. Attention commencer la preuve de réception avant d’ouvrir le colis</p>'
                        });
                    }
                });
            };

            $scope.confirmProofRecept = function() {
                var toUser = isOwner() ? $scope.receiptProfile : $scope.ownerProfile;
                var confirmPopup = $ionicPopup.confirm({
                    clickOutsideToClose: true,
                    title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                    cssClass: "popup_1",
                    okText: "VALIDER",
                    cancelText: "DECLINER",
                    template: '<p class="center">' + $translate.instant("POPUP_ACCEPTE") + '</p>'
                });
                confirmPopup.then(function(res) {
                    if (res) {

                    	firebase.database().ref('videos/' + $stateParams.slug).update({
                            validate: 1,
                            validatedAt: firebase.database.ServerValue.TIMESTAMP
                        });

                        var msgData = {
                            videoId: $stateParams.slug,
                            type: "confirm_proof_recept",
                            fromId: $rootScope.currentUser.uid,
                            updatedAt: firebase.database.ServerValue.TIMESTAMP
                        };
                        var database = firebase.database();
                        var newMsgKey = firebase.database().ref().child('proof_info').push().key;
                        database.ref('proof_info/' + newMsgKey).set(msgData);
                        var message = "La preuve de réception " + $scope.firstVid.title + " a été validé par " + $scope.firstVid.receipt_name;
                        $scope.saveNotification(message, toUser.key);

                        $ionicPopup.alert({
                            clickOutsideToClose: true,
                            title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                            cssClass: "popup_1",
                            okText: "Fermer",
                            template: '<p class="center">La preuve de réception a été validé</p>'
                        });
                    } else {
                        $scope.data = {valid: true};
                        var confirmPopup2 = $ionicPopup.confirm({
                            scope: $scope,
                            clickOutsideToClose: true,
                            title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                            cssClass: "popup_1",
                            okText: "DECLINER",
                            cancelText: "ANNULER",
                            template: '<p class="center"><b>Merci de préciser le motif de déclinaison de cette preuve.</b></p>'+
                                        '<select class="item item-select" id="refuseInfo" ng-model="data.refuseInfo" style="width: 100%">' +
	                                    '    <option value="">Préciser le motif</option>' +                                        
	                                    '    <option value="La Preuve ne respecte pas les règles de capture vidéo ZeOkay">La Preuve ne respecte pas les règles de capture vidéo ZeOkay</option>' +
	                                    '    <option value="Non concerné par cette preuve">Non concerné par cette preuve</option>' +
	                                    '    <option value="Vidéo non visible">Vidéo non visible</option>' +
                                        '</select>'
                            }, $scope);

                        confirmPopup2.then(function(res) {
                            if (res) {

                            	if(!$scope.data.refuseInfo) {
                            		Utils.message(Popup.errorIcon, "Merci de préciser la raison du rejet !");                            		
                            	} else {

                            		firebase.database().ref('videos/' + $stateParams.slug).update({
			                            validate: 0,
			                            validatedAt: firebase.database.ServerValue.TIMESTAMP
			                        });

	                                var msgData = {
	                                    videoId: $stateParams.slug,
	                                    type: "refuse_proof_recept",
	                                    refuseInfo: $scope.data.refuseInfo,
	                                    fromId: $rootScope.currentUser.uid,
	                                    updatedAt: firebase.database.ServerValue.TIMESTAMP
	                                };
	                                var database = firebase.database();
	                                var newMsgKey = firebase.database().ref().child('proof_info').push().key;
	                                database.ref('proof_info/' + newMsgKey).set(msgData);
	                                var message = "La preuve de réception " + $scope.firstVid.title + " a été décliné par " + $scope.firstVid.receipt_name;
	                                $scope.saveNotification(message, toUser.key);

	                                $ionicPopup.alert({
	                                    clickOutsideToClose: true,
	                                    title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
	                                    cssClass: "popup_1",
	                                    okText: "Fermer",
	                                    template: '<p class="center">La preuve de récéption a été décliné</p>'
	                                });
                            	}
                            }
                        });
                    }
                });
            };

            $scope.validateProof = function() {                
                var confirmPopup = $ionicPopup.confirm({
                    clickOutsideToClose: true,
                    title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                    cssClass: "popup_1",
                    okText: "VALIDER",
                    cancelText: "DECLINER",
                    template: '<p class="center">' + $translate.instant("POPUP_ACCEPTE") + '</p>'
                }, $scope);
                confirmPopup.then(function(res) {
                    if (res) {
                        firebase.database().ref('videos/' + $stateParams.slug).update({
                            validate: 1,
                            validatedAt: firebase.database.ServerValue.TIMESTAMP
                        });
                        var msgData = {
                            videoId: $stateParams.slug,
                            type: "validate",
                            fromId: $rootScope.currentUser.uid,
                            updatedAt: firebase.database.ServerValue.TIMESTAMP
                        };
                        var database = firebase.database();
                        var newMsgKey = firebase.database().ref().child('proof_info').push().key;
                        database.ref('proof_info/' + newMsgKey).set(msgData);
                        var message = "Votre preuve " + $scope.firstVid.title + " est validée par " + $scope.firstVid.receipt_name;
                        $scope.saveNotification(message, $scope.firstVid.ownerUid);

                        $ionicPopup.alert({
                            clickOutsideToClose: true,
                            title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                            cssClass: "popup_1",
                            okText: "Fermer",
                            template: '<p class="center">La preuve a été validé</p>'
                        });
                    } else {
                        $scope.data = {valid: true};
                        var confirmPopup2 = $ionicPopup.confirm({
                            scope: $scope,
                            clickOutsideToClose: true,
                            title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                            cssClass: "popup_1",
                            okText: "DECLINER",
                            cancelText: "ANNULER",
							template: '<p class="center"><b>Merci de préciser le motif de déclinaison de cette preuve.</b></p>' +
									'  <select class="item item-select" id="refuseInfo" ng-model="data.refuseInfo" style="width: 100%">' +
									' 		<option value="">Préciser le motif</option>' +
									' 		<option value="Colis susceptible d’être modifié">Colis susceptible d’être modifié</option>' +
									'		<option value="La Preuve ne respecte pas les règles de capture vidéo ZeOkay">La Preuve ne respecte pas les règles de capture vidéo ZeOkay</option>' +
									'		<option value="Non concerné par cette preuve">Non concerné par cette preuve</option>' +
									'		<option value="Article non conforme ou manquant">Article non conforme ou manquant</option>' +
									'		<option value="Vidéo non visible">Vidéo non visible</option>' +
									'</select>'
                        }, $scope);

                        confirmPopup2.then(function(res) {
                            if (res) {

                            	if(!$scope.data.refuseInfo)
                            	{
                            		Utils.message(Popup.errorIcon, "Merci de préciser la raison du rejet !");
                            	} else {
	                                firebase.database().ref('videos/' + $stateParams.slug).update({
	                                    validate: 0,
	                                    refuseInfo: $scope.data.refuseInfo,
	                                    refusedAt: firebase.database.ServerValue.TIMESTAMP
	                                });
	                                var msgData = {
	                                    videoId: $stateParams.slug,
	                                    type: "refuse",
	                                    refuseInfo: $scope.data.refuseInfo,
	                                    fromId: $rootScope.currentUser.uid,
	                                    updatedAt: firebase.database.ServerValue.TIMESTAMP
	                                };
	                                var database = firebase.database();
	                                var newMsgKey = firebase.database().ref().child('proof_info').push().key;
	                                database.ref('proof_info/' + newMsgKey).set(msgData);

	                                var message = "Votre preuve " + $scope.firstVid.title + " est declinée par " + $scope.firstVid.receipt_name;
	                                $scope.saveNotification(message, $scope.firstVid.ownerUid);
	                                $ionicPopup.alert({
	                                    clickOutsideToClose: true,
	                                    title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
	                                    cssClass: "popup_1",
	                                    okText: "Fermer",
	                                    template: '<p class="center">La preuve a été décliné</p>'
	                                });
	                            }
                            }
                        });
                    }
                });
            };

            // An alert dialog
            $scope.showAlert = function(title, template) {
                var alertPopup = $ionicPopup.alert({
                    title: title,
                    template: template
                });
            };

            $scope.openVideo = function(video) { 
                var fromId = video.fromId;
                if (fromId && fromId !== $rootScope.currentUser.uid) {
                    //alert(fromId);
                    var message = "Votre preuve " + $scope.firstVid.title + " est visualisée par " + $rootScope.currentProfile.username;
                    var log = "Preuve visualisée par " + $rootScope.currentProfile.username;
                    var msgData = {
                        videoId: $stateParams.slug,
                        type: "message",
                        fromId: $rootScope.currentUser.uid,
                        updatedAt: firebase.database.ServerValue.TIMESTAMP,
                        message: log
                    };
                    firebase.database().ref().child('proof_info').push(msgData);
                    $scope.saveNotification(message, fromId);
                }                
                $scope.link = video.url;
                Utils.video(video);
            };

            $scope.showInfoProof = function() {
                $scope.searchUserList = false;
                $scope.infoProof = true;
            };
            $scope.showInfoProof();

            $scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
            };

            var savePicture = function(url) {
                var msgData = {
                    videoId: $stateParams.slug,
                    type: "photo",
                    fromId: $rootScope.currentUser.uid,
                    updatedAt: firebase.database.ServerValue.TIMESTAMP,
                    url: url
                };
                var database = firebase.database();
                var newMsgKey = firebase.database().ref().child('proof_info').push().key;
                database.ref('proof_info/' + newMsgKey).set(msgData);
                var message = "Nouvelle photo pour la preuve " + $scope.firstVid.title;
                var userUid = $scope.firstVid.receiptUid;
                if ($scope.firstVid.receiptUid == $rootScope.currentUser.uid) {
                    userUid = $scope.firstVid.ownerUid;
                }
                $scope.saveNotification(message, userUid);
            };


            $scope.takePicture = function() {
                if ($rootScope.currentFormula.nbr_photos > $scope.countPicture) {
                        Utils.getPicture(Camera.PictureSourceType.CAMERA);
                } else {
                    $ionicPopup.alert({
                        clickOutsideToClose: true,
                        title: "<img class='icon-popup' src='img/icons/confirm_icon.png' />",
                        cssClass: "popup_1",
                        okText: "Fermer",
                        template: '<p class="center">Vous avez atteint le nombre max de photo pour cette preuve.</p>'
                    });
                }
            };
             //Broadcast from our Utils.getPicture function that tells us that the image selected has been uploaded.
            $scope.$on('imageUploaded', function(event, args) {
                //Proceed with sending of image message.
                savePicture(args.imageUrl);
                Utils.hide();
            });
            $scope.prepareMessage = function() {
                $scope.activeMsgDiv = true;
            };

            $scope.sendMessage = function(message) {
                $scope.data = {};
                var toUserId = isOwner() ? $scope.receiptProfile : $scope.ownerProfile;
                if (!message) {
                    $scope.activeMsgDiv = false;
                    e.preventDefault();
                } else {
                    ////console.log($scope.data.message);
                    var msgData = {
                        videoId: $stateParams.slug,
                        type: "message",
                        fromId: $rootScope.currentUser.uid,
                        updatedAt: firebase.database.ServerValue.TIMESTAMP,
                        message: message
                    };
                    var database = firebase.database();
                    var newMsgKey = firebase.database().ref().child('proof_info').push().key;
                    database.ref('proof_info/' + newMsgKey).set(msgData);
                    var message = "Vous avez reçu un message au sujet de votre preuve " + $scope.firstVid.title;                    
                    $scope.saveNotification(message, toUserId.key);                    
                    this.message = "";
                }
            };

            $scope.showSetting = function() {
                $scope.goTo("/setting");
            };

            $scope.selectUser = function(user) {
                $rootScope.sendUser = user;
                $scope.showInfoProof();
                ////console.log(user);
            };

            /*
             * go to url
             */
            $scope.$on('$ionicView.beforeEnter', function() {
            	console.log('$ionicView.beforeEnter');
            });
             
            $scope.$on("$ionicView.loaded", function( scopes, states ) {
             	console.log('$ionicView.loaded');
		        
		        // Anything you can think of
		        $scope.getVideos();
		        $scope.getVideoInfos();
		          
            });

            $scope.goTo = function(path) {
                if(path === 'home' || path === '/home') 
                    firebase.database().ref("proof_info").orderByChild('videoId').startAt($stateParams.slug).endAt($stateParams.slug).off();
                $location.path(path);
            };

            $scope.enlargeImage = function(url) {
                Utils.image(url);
            };

            $scope.deleteProof = function(url) {
                $rootScope.deleteVideo($scope.infoVideo).then(function() {
                    $scope.popover.hide();
                    $scope.goTo("home");
                });
            };

            //Scroll to bottom so new messages will be seen.
            $scope.scrollBottom = function() {
                $timeout(function() {
                    if($location.$$path.indexOf('proofdetail') === 1)
                        $ionicScrollDelegate.scrollBottom(true);
                }, 500);
            };

            //Scroll to top.
            $scope.scrollTop = function() {
                $ionicScrollDelegate.scrollTop(true);
            };

            $scope.getAvatarProfile = function () {
                var avatar;
                if ($scope.ownerProfile && $scope.receiptProfile) {
                    if (!isOwner())
                        avatar = $scope.ownerProfile;
                    else
                        avatar = $scope.receiptProfile;
                }
                return avatar;
            };

            var isOwner = function() {
                return $scope.firstVid && $rootScope.currentUser && $rootScope.currentUser.uid === $scope.firstVid.ownerUid;
            };

            //  $scope.saveVideo($stateParams.slug);
            ////console.log($stateParams.slug);
        }
    ]);
})();