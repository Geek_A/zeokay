(function() {
    'use strict';
    zeokey.controller('SettingCtrl', ['$scope', '$rootScope', '$location', '$ionicHistory', '$sce', 'Utils',
        function($scope, $rootScope, $location, $ionicHistory, $sce, Utils) {


            $scope.priviousUrl = "home";
            // An alert dialog
            $rootScope.isLogged = false;
            $scope.goTo = function(path) {
                $location.path(path);
            };
            $scope.logout = function() {
                logoutFirebase();
                if (window.plugins && window.plugins.googleplus)
                    window.plugins.googleplus.logout();

                if (window.plugins && facebookConnectPlugin)
                    facebookConnectPlugin.logout();

            };

            var logoutFirebase = function() {
                var uid = $rootScope.currentUid;
                firebase.database().ref('users/' + uid).update({
                    deviceId: ''
                });
                firebase.auth().signOut().then(Utils.clearUserDate()).then($scope.goTo('login'));
            };
        }
    ]);
})();