(function () {
    'use strict';
    zeokey.controller('PaiementCtrl', ['$scope', '$rootScope', '$location', '$ionicPopup',
        function ($scope, $rootScope, $location, $ionicPopup) {
            $scope.goTo = function (path) {
                $location.path(path);
            };

            $scope.cardType = {};
            $scope.card = {};

            $scope.makeStripePayment = makeStripePayment;


            function makeStripePayment(_cardInformation) {
                //console.log("makeStripePayment", _cardInformation);

                if (!window.stripe) {
                    alert("stripe plugin not installed");
                    return;
                }

                if (!_cardInformation) {
                    alert("Invalid Card Data");
                    return;
                }
                stripe.charges.create({
                    // amount is in cents so * 100
                    amount: $rootScope.amountToPay * 100,
                    currency: 'usd',
                    card: {
                        "number": _cardInformation.number,
                        "exp_month": _cardInformation.exp_month,
                        "exp_year": _cardInformation.exp_year,
                        "cvc": '123',
                        "name": "Aaron Saunders"
                    },
                    description: "Stripe Test Charge"
                },
                function (response) {
                    //console.log(JSON.stringify(response, null, 2));
                    // alert(JSON.stringify(response, null, 2));
                    if (response.paid) {
                        //alert("paiement avec succés");
                       // firebase.auth().onAuthStateChanged(function (user) {
                       var user = $rootScope.currentUser;
                            if (user) {
                                firebase.database().ref('users/' + user.uid).update({
                                    subscription: $rootScope.subsciptionId,
                                    maxProof: $rootScope.maxProof
                                });
                                var alertPopup = $ionicPopup.alert({
                                    title: "Paiement accepté",
                                    template: "Le paiement est accepté !"
                                });
                                $location.path("home");
                            }
                       // });
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: "Erreur",
                            template: "Erreur de paiement, Veuillez réessayer"
                        });
                        //error
                    }
                    //save subsciption
                }, function (response) {
                    alert(JSON.stringify(response))
                }
                );
            }


        }
    ]);
})();
