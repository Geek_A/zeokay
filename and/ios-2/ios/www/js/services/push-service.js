zeokey.config(['$provide',
    function($provide) {
        $provide.factory('PushServices', ['$http', '$q', '$filter', '$location', '$rootScope',
            function($http, $q, $filter, $location, $rootScope) {
                return {
                    sendPush: function(deviceId, message, state) {
                        var deferred = $q.defer();
                        var payload = {"$state": state};
                        datas = {
                            "tokens": [deviceId],
                            "profile": "zeokey",
                            "notification": {
                                "title": "Zeokey",
                                "message": message,
                                "android": {
                                    "payload": payload
                                },
                                "ios": {
                                    "badge": 1,
                                    "payload": payload
                                }
                            }

                        };
                        $http.defaults.headers.common.Authorization = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3ODEyYjk5My0wMGU0LTQ0ZmYtOTEzZi0xMzFkMTk1MjBkOWQifQ.NE5gFYxI7VA4o5cBU-OtmhVRICQYER0J5Js0_p-6YWM';
                        $http.post('https://api.ionic.io/push/notifications', datas).success(function(data, status, headers, config) {
                            deferred.resolve(data);
                        }).error(function() {
                            deferred.resolve(false);
                        });
                        return deferred.promise;
                    }
                };
            }
        ]);
    }
]);
