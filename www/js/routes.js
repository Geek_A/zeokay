angular.module('zeokey').config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'templates/dashboard.html',
                controller: 'DashboardCtrl'
            })
            .state('cgu', {
                url: '/cgu',
                templateUrl: 'templates/cgu.html',
                controller: 'AboutCtrl'
            })
            .state('cgv', {
                url: '/cgv',
                templateUrl: 'templates/cgv.html',
                controller: 'AboutCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            })
            .state('reset-password', {
                url: '/reset-password',
                templateUrl: 'templates/reset-password.html',
                controller: 'ResetPasswordCtrl'
            })
            .state('home_slug', {
                url: '/home/:slug',
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
            })
            .state('home', {
                url: '/home',
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
            })

            .state('register', {
                url: '/register',
                templateUrl: 'templates/register.html',
                controller: 'RegisterCtrl'

            })
            .state('proof', {
                url: '/proof/:slug/:capture',
                templateUrl: 'templates/proof.html',
                controller: 'ProofCtrl'
            })
            .state('proof_detail', {
                url: '/proofdetail/:slug',
                templateUrl: 'templates/proofdetail.html',
                controller: 'ProofDetailCtrl'

            })
            .state('message', {
                url: '/message/:slug',
                templateUrl: 'templates/message.html',
                controller: 'MessageCtrl'

            })
            .state('setting', {
                url: '/setting',
                templateUrl: 'templates/setting.html',
                controller: 'SettingCtrl'

            })
            .state('profil', {
                url: '/profil',
                templateUrl: 'templates/profil.html',
                controller: 'ProfilCtrl'

            })
            .state('card', {
                url: '/card',
                templateUrl: 'templates/card.html',
                controller: 'CardCtrl'

            })
            .state('addpayment', {
                url: '/addpayment',
                templateUrl: 'templates/add-paiement.html',
                controller: 'CardCtrl'

            })
            .state('help', {
                url: '/help',
                templateUrl: 'templates/help.html',
                controller: 'HelpCtrl'

            })
            .state('subscription', {
                url: '/subscription',
                templateUrl: 'templates/subscription.html',
                controller: 'SubscriptionCtrl'

            })
            .state('recap', {
                url: '/recap',
                templateUrl: 'templates/recap.html',
                controller: 'RecapCtrl'

            }).state('unitbuy', {
                url: '/unitbuy',
                templateUrl: 'templates/unitbuy.html',
                controller: 'UnitBuyCtrl'

            }).state('download', {
                url: '/download/:slug',
                templateUrl: 'templates/download_proof.html',
                controller: 'DownloadCtrl'

            }).state('paiement', {
                url: '/paiement',
                templateUrl: 'templates/paiement.html',
                controller: 'PaiementCtrl'

            })
            .state('formulas', {
                url: '/formulas',
                templateUrl: 'templates/formula.html',
                controller: 'FormulaCtrl'

            })
            .state('contact', {
                url: '/contact',
                templateUrl: 'templates/contact.html',
                controller: 'ContactCtrl'

            })
            .state('inbox', {
                url: '/inbox',
                templateUrl: 'templates/inbox.html',
                controller: 'InboxCtrl'

            })
            .state('search', {
                url: '/search',
                templateUrl: 'templates/search.html',
                controller: 'SearchCtrl'

            })
            .state('user', {
                url: '/user/:slug',
                templateUrl: 'templates/user.html',
                controller: 'UserCtrl'

            })
            .state('notification', {
                url: '/notification',
                templateUrl: 'templates/notification.html',
                controller: 'NotificationCtrl'
            })
            .state('about', {
                url: '/about',
                templateUrl: 'templates/about.html',
                controller: 'AboutCtrl'

            });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/dashboard');
});
