var zeokey = {};
(function() {
    zeokey = angular.module('zeokey', ['ionic', 'ionicImgCache', 'firebase', 'angular.filter', 'ionic.service.push', 'ngCookies',
        'pascalprecht.translate', 'ngCordova', 'ngMessages', 'ngVideo', 'ion-autocomplete', 'angularMoment', 'ngCordovaOauth',
         'ngCordova.plugins.cardIO', 'angularCircularNavigation', 'pr.longpress', 'angular.chips', 
        'angular-progress-arc', 'credit-cards']);
    zeokey.filter('trustAsResourceUrl', ['$sce', function($sce) {
        return function(val) {
            return $sce.trustAsResourceUrl(val);
        };
    }]);
    zeokey.run(function($ionicPlatform, $cordovaLocalNotification, $http, $rootScope, $cordovaKeyboard, $interval, $translate, $ionicPopup, $location, $state, $timeout, $ionicPush, Utils, Popup, StripeConfig) {
        
        $ionicPlatform.ready(function() {
            //Code update live
            if (window.cordova && window.cordova.plugins) {
                if( window.codePush) {
                    window.codePush.checkForUpdate(function(update) {
                        if (!update) {
                            //console.log("The app is up to date.");
                        } else {
                            window.codePush.sync(function(syncStatus) {
                                switch (syncStatus) {
                                    case SyncStatus.APPLY_SUCCESS:
                                        //Success
                                        Utils.message("ion-ios-checkmark-outline balanced", "L'application est à jour.");
                                        return;
                                    case SyncStatus.UP_TO_DATE:
                                        // displayMessage("L'application est à jour.");
                                        break;
                                    case SyncStatus.UPDATE_IGNORED:
                                        Utils.message("ion-ios-checkmark-outline balanced", "Vous avez décidé de ne pas installer la mise à jour optionnelle.");
                                        break;
                                    case SyncStatus.ERROR:
                                        Utils.message("ion-ios-close-outline energized", "Une erreur s'est produite lors de la recherche de mises à jour.");
                                        break;
                                }
                            }, { updateDialog: { updateTitle: "Une mise à jour est disponible! Devrions-nous la télécharger ?" } });
                        }
                    });
                }

                /* End Set data */

                if (window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(false);
                }

                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
                
                console.log(cordova.plugins.stripe);
                
                if(cordova.plugins.stripe) {
                    
                    cordova.plugins.stripe.setPublishableKey(StripeConfig.stripeAppId);
                }

                
                var push = new Ionic.Push({
                    "debug": true,
                    "onNotification": function(notification) {
                        var message = notification._raw.message;
                        var state = notification._raw.additionalData.payload.$state;
                        var sleep = notification.app.asleep;
                        if (state) {
                            if (sleep) {
                                $location.path(state);
                            } else {
                                //Utils.toast(message);                                                              
                            }
                        }
                    }
                });

                push.register(function(token) {
                    $rootScope.deviceId = token._token;
                    push.saveToken(token);
                }, function(error) {
                    $rootScope.deviceId = '';
                    //console.log(error);
                });
                
            }
        });
        // redirect to login page if not logged in and trying to access a restricted page
        $rootScope.$on('$locationChangeStart', function(event, next, current) {
            var publicPages = ['/login', '/reset-password', '/register', '/dashboard'];
            firebase.auth().onAuthStateChanged(function(user) {
                $rootScope.currentUser = user;
                if (user) {
                    firebase.database().ref('users/' + user.uid).on('value', function(profile) {
                        $rootScope.currentProfile = profile.val();
                    });
                    if (publicPages.indexOf($location.path()) > -1) {
                        $location.path("home");
                    }
                } else {
                    if (publicPages.indexOf($location.path()) === -1) {
                        $location.path("login");
                    }
                }
            });
        });

        //Monitor connection
        var connectedRef = firebase.database().ref(".info/connected");
        connectedRef.on("value", function(snap) {
            if (snap.val() === true) {
                Utils.message(Popup.successIcon, $translate.instant("WELCOME"));
            } else {
                Utils.conneting();
            }
        });

        /* Initial configuration */
        $rootScope.imgMenu = "img/icons/zeokay.png";
        $rootScope.imgMenuRed = "img/icons/zeokay-red.png";
        $rootScope.progress = 0;

        // Init data service
        $rootScope.videos = [];
        $rootScope.receiptvideos = [];
        $rootScope.listFriends = [];
        $rootScope.fromMeConversations = [];
        $rootScope.toMeConversations = [];
        $rootScope.listFriends = [];
        $rootScope.listUsers = {};
        $rootScope.notifications = [];
        $rootScope.sources = [];

        // get the current user formula informations
        firebase.auth().onAuthStateChanged(function(user) {
            $rootScope.currentUser = user;
            if (user) {
                //Utils.show();
                $rootScope.currentUid = user.uid;

                //get formilas
                firebase.database().ref('formulas').on('value', function(snapshot) {
                    var values = snapshot.val() ? Object.keys(snapshot.val()).map(function(key) { return snapshot.val()[key]; }) : [];
                    $rootScope.formulas = values.sort(function(a, b) {
                        return b.price.month - a.price.month;
                    });
                });
                
                firebase.database().ref('stripe_customers/' + user.uid + '/sources').on('value', function(snapshot) {  
                    var obj = snapshot.val() ? snapshot.val() : [];                  
                    $rootScope.sources = Object.keys(obj).map(function(key) {
                        obj[key].key = key;
                        return obj[key];
                    }).filter(function(el) {
                        return el.id != undefined;
                    });

                    $rootScope.$apply();
                });
                   
                // get current user profile
                firebase.database().ref('users/' + user.uid).on('value', function(profile) {
                    $rootScope.currentProfile = profile.val();
                    if ($rootScope.currentProfile && $rootScope.currentProfile.subscription)
                        firebase.database().ref('formulas/' + $rootScope.currentProfile.subscription.formula_id).on('value', function(formula) {
                            $rootScope.currentFormula = formula.val();
                            $rootScope.progress = $rootScope.currentProfile.subscription.remaining_proofs / $rootScope.currentFormula.nbr_proof;
                        });
                });

                //get all users
                firebase.database().ref('users').on('value', function(snapshot) {
                    var obj = snapshot.val() ? snapshot.val() : [];
                    Object.keys(obj).map(function(key) {
                        obj[key].key = key;

                    });
                    $rootScope.listUsers = obj;
                });

                //get all conversations
                var ref = firebase.database().ref("conversations");
                ref.orderByChild('fromId').startAt(user.uid).endAt(user.uid).on("value", function(snapshot) {
                    var obj = snapshot.val() ? snapshot.val() : [];
                    $rootScope.fromMeConversations = obj ? Object.keys(obj).map(function(key) {
                        obj[key].key = key;
                        return obj[key];
                    }) : [];
                    angular.forEach(snapshot.val(), function(f, key) {
                        var item = f;
                        item.key = key;
                        item.conversation = item.key;
                        item.uid = item.toId;
                        item.userProfile = $rootScope.listUsers[item.toId];;
                        Utils.checkAndAdd($rootScope.listFriends, item);
                    });
                });
                ref.orderByChild('toId').startAt(user.uid).endAt(user.uid).on("value", function(snapshot) {
                    var obj = snapshot.val() ? snapshot.val() : [];
                    $rootScope.toMeConversations = obj ? Object.keys(obj).map(function(key) {
                        obj[key].key = key;
                        return obj[key];
                    }) : [];
                    angular.forEach(snapshot.val(), function(f, key) {
                        var item = f;
                        item.key = key;
                        item.conversation = item.key;
                        item.uid = item.toId;
                        item.userProfile = $rootScope.listUsers[item.fromId];
                        Utils.checkAndAdd($rootScope.listFriends, item);
                    });
                });
                ref.on('child_removed', function(change) {
                    Utils.checkAndRemove($rootScope.listFriends, change.key);
                });


                /* Watch notifications */
                var refNotif = firebase.database().ref("notifications");
                refNotif.orderByChild('user').startAt(user.uid).endAt(user.uid).on("value", function(data) {
                    $timeout(function() {
                        var value = data.val() ? data.val() : [];
                        var notifications = Object.keys(value).map(function(key) {
                            value[key].key = key;
                            return value[key];
                        });
                        angular.forEach(notifications, function(notif, key) {
                            notif.userProfile = $rootScope.listUsers[notif.fromUser];
                        });
                        $rootScope.notifications = notifications;
                    });
                });
                refNotif.on('child_removed', function(change) {
                    Utils.checkAndRemove($rootScope.notifications, change.key);
                });
                var first = true;
                refNotif.orderByChild('user').startAt(user.uid).endAt(user.uid).limitToLast(1).on('child_added', function (change) {
                    if (first) {
                        first = false;
                    } else {
                        Utils.toast(change.val().message);
                    }
                });


                //Watch all videos
                var refVideos = firebase.database().ref("videos");
                refVideos.orderByChild('send_user').startAt(user.email).endAt(user.email).on('value', function(snapshot) {
                     firebase.database().ref("/.info/serverTimeOffset").on('value', function(offset) {
                        var offsetVal = offset.val() || 0;
                        var serverTime = Date.now() + offsetVal;
                        $timeout(function() {
                            var childKey = snapshot.val() ? snapshot.val() : [];
                            angular.forEach(childKey, function(video, key) {
                                video.key = key;
                                if (video.owner_delete === 0 && (!video.archiveAt || video.archiveAt > serverTime) ) {                                    
                                    Utils.checkAndAdd($rootScope.videos, video);
                                } else {
                                    Utils.checkAndRemove($rootScope.videos, video);
                                }
                            });
                        });
                    });
                });
                refVideos.orderByChild('receipt_user').startAt(user.email).endAt(user.email).on('value', function(snapshot) {
                    firebase.database().ref("/.info/serverTimeOffset").on('value', function(offset) {
                        var offsetVal = offset.val() || 0;
                        var serverTime = Date.now() + offsetVal;
                        $timeout(function() {
                            var childKey = snapshot.val() ? snapshot.val() : [];
                            angular.forEach(childKey, function(video, key) {
                                video.key = key;
                                if (video.recieve_delete === 0 && ( !video.archiveAt || video.archiveAt > serverTime) ) {                                    
                                    Utils.checkAndAdd($rootScope.receiptvideos, video);
                                } else {
                                    Utils.checkAndRemove($rootScope.receiptvideos, video);
                                }
                            });
                        });
                    });
                });

                refVideos.on('child_removed', function(change) {
                    Utils.checkAndRemove($rootScope.listFriends, change.key);
                });
            }
        });

    }).config(function($stateProvider, $compileProvider, $urlRouterProvider, $translateProvider, $ionicConfigProvider, progressArcDefaultsProvider, ionicImgCacheProvider, FirebaseConfig) {

        if (ionic.Platform.isAndroid()) {
            $ionicConfigProvider.scrolling.jsScrolling(false);
        }

        // Enable debug
        $compileProvider.debugInfoEnabled(false);

        // Set storage size quota to 100 MB.
        ionicImgCacheProvider.quota(100);

        // Set foleder for cached files.
        ionicImgCacheProvider.folder('.zeokay');

        progressArcDefaultsProvider
            .setDefault('background', 'rgba(248, 248, 248, 0)')
            .setDefault('size', 300);
        $ionicConfigProvider.views.transition('android');
        var config = {
            apiKey: "AIzaSyAV3rCFwgu7T-cKrbqXpYHGww8rdoKZWJ8",
            authDomain: "zeokey-b2fe6.firebaseapp.com",
            databaseURL: "https://zeokey-b2fe6.firebaseio.com",
            storageBucket: "zeokey-b2fe6.appspot.com",
            messagingSenderId: "625247332461"
        };
        /*var config = {
         apiKey: "AIzaSyBF8jInS4cxMrHnYWLw7LunHTVaV8Xzt9U",
         authDomain: "zeokey-test.appspot.com",
         databaseURL: "https://zeokey-test.firebaseio.com",
         storageBucket: "zeokey-test.appspot.com",
         messagingSenderId: "1025261251810"
         };/**/
        firebase.initializeApp(config);
        $ionicConfigProvider.tabs.position('bottom');

        for (lang in translations) {
            if (lang) {
                $translateProvider.translations(lang, translations[lang]);
            }
        }

        if (window.localStorage['NG_TRANSLATE_LANG_KEY'] == undefined || !window.localStorage['NG_TRANSLATE_LANG_KEY']) {
            var useLang = 'EN';
            var nav = window.navigator,
                browserLanguagePropertyKeys = [
                    'language',
                    'browserLanguage',
                    'systemLanguage',
                    'userLanguage'
                ],
                i, language;
            if (angular.isArray(nav.languages)) {
                for (i = 0; i < nav.languages.length; i++) {
                    language = nav.languages[i];
                    if (language && language.length) {
                        useLang = language;
                    }
                }
            }
            for (i = 0; i < browserLanguagePropertyKeys.length; i++) {
                language = nav[browserLanguagePropertyKeys[i]];
                if (language && language.length) {
                    useLang = language;
                }
            }

            var firstLang = 'EN';
            var twoLetterISO = useLang.substring(0, 2).toUpperCase();
            if (twoLetterISO == 'ES' && useLang.length > 2) {
                firstLang = 'ESLA';
            } else if (twoLetterISO == 'DE' || twoLetterISO == 'RU' || twoLetterISO == 'TR' || twoLetterISO == 'FR' || twoLetterISO == 'PT' || twoLetterISO == 'IT' || twoLetterISO == 'ES' || twoLetterISO == 'HI' || twoLetterISO == 'JA' || twoLetterISO == 'ZH' || twoLetterISO == 'KO') {
                firstLang = twoLetterISO;
            }

            $translateProvider.preferredLanguage(firstLang);
            $translateProvider.useLocalStorage();
        } else {
            $translateProvider.preferredLanguage('EN');
            $translateProvider.useLocalStorage();
        }
        $translateProvider.useSanitizeValueStrategy('escape');
    }).directive('stopEvent', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                element.bind('click', function(e) {
                    e.stopPropagation();
                });
            }
        };
    }).directive('capitalize', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, modelCtrl) {
                var capitalize = function(inputValue) {
                    if (inputValue == undefined)
                        inputValue = '';
                    var capitalized = inputValue.toLowerCase();
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                    }
                    return capitalized;
                }
                modelCtrl.$parsers.push(capitalize);
                capitalize(scope[attrs.ngModel]); // capitalize initial value
            }
        };
    });
})();