(function() {
    'use strict';
    zeokey.controller('NotificationCtrl', ['$scope', '$rootScope', '$location', '$ionicPopup', '$ionicListDelegate', '$translate', '$filter', '$timeout', '$ionicLoading', '$ionicHistory', 'Utils',
        function($scope, $rootScope, $location, $ionicPopup, $ionicListDelegate, $translate, $filter, $timeout, $ionicLoading, $ionicHistory, Utils) {
            $scope.priviousUrl = "home";
            $scope.data = {
                showDelete: false
            };
            /*
             * go to url
             */
            $scope.goTo = function(path, notification) {
                if (notification && notification.key && notification.read == 0) {
                    firebase.database().ref('notifications/' + notification.key).update({
                        read: 1
                    });
                }
                $location.path(path);
            };
            $scope.deleteNotification = function(notification) {
                firebase.database().ref('notifications/' + notification.key).remove().then(function() {
                    $ionicListDelegate.closeOptionButtons();
                });
            };
            $scope.setRead = function(notification) {
                firebase.database().ref('notifications/' + notification.key).update({
                    read: 1
                }).then(function() {
                    $ionicListDelegate.closeOptionButtons();
                });;
            };
        }
    ]);
})();