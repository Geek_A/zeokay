(function() {
    'use strict';
    zeokey.controller('DashboardCtrl', ['$scope', '$rootScope', '$ionicLoading', '$timeout', '$location', '$window', '$ionicSlideBoxDelegate', function($scope, $rootScope, $ionicLoading, $timeout, $location, $window, $ionicSlideBoxDelegate) {
        //console.log("dashboard");
        $rootScope.isLogged = false;
        $scope.show = function() {
            $scope.showSlider = false;
            // Get a reference to the database service
            var ref = firebase.database().ref("slide_dashboard");
            ref.once("value").then(function(snapshot) {
                var childKey = snapshot.val();
                angular.forEach(childKey, function(slide, key) {
                    // console.log(slide);
                    $scope.sliders = [];
                    var pathReference = firebase.storage().ref('slider_dashboard/');
                    pathReference.child(slide.image).getDownloadURL().then(function(url) {
                        $scope.sliders.push({
                            title: slide.title,
                            description: slide.description,
                            image: url
                        });

                        $ionicSlideBoxDelegate.update();
                        $scope.$apply();
                    }).catch(function(error) {
                        // Handle any errors
                    });
                });
                $scope.showSlider = true;
                document.getElementById("custom-overlay").style.display = "none";
                //$scope.$apply();
            });
            /*  $ionicLoading.show({
             template: '<img class="loading-img" src="img/loading.png">'
             });*/
        };
        /*
         * go to url
         */
        $scope.goTo = function(path) {
            $location.path(path);
        };
        // $scope.show($ionicLoading);
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                $location.path("home");
            } else {
                $scope.show($ionicLoading);
            }
        });

        setTimeout(function() {
            var element = document.getElementById("custom-overlay");
            if (element) { element.style.display = "none"; }
        }, 15000);

    }]);
})();