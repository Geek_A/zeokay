(function() {
    'use strict';
    zeokey.controller('InboxCtrl', ['$scope', '$rootScope', '$location', '$ionicPopup', '$translate', '$timeout', '$ionicHistory', '$ionicLoading', 'DataService', 'Utils',
        function($scope, $rootScope, $location, $ionicPopup, $translate, $timeout, $ionicHistory, $ionicLoading, DataService, Utils) {
            $scope.imgMenu = "img/icons/inbox-btn.png";
            $scope.imgMenuRed = "img/icons/inbox-btn.png";
            //Set mode to Messages.
            $scope.mode = 'Messages';
            $scope.listFriends = [];
            $scope.state = $ionicHistory.currentStateName();
            $scope.selectUser = function(user) {
                $rootScope.messageUser = user;
                $location.path("message/test");
            };

            $scope.priviousUrl = "home";
            if ($ionicHistory.backView()) {
                $scope.priviousUrl = $ionicHistory.backView().url;
            }

            $scope.goTo = function(path) {
                $location.path(path);
            };

            //Change mode to Compose message.
            $scope.actionMenu = function() {
                $scope.mode = $scope.mode == 'Compose' ? 'Messages' : 'Compose';                
            };

            //Change mode to view messages.
            $scope.cancel = function() {
                $scope.mode = 'Messages';
            };

            $scope.hasUpdates = function(conversation) {
                if (conversation.fromId === $rootScope.currentUid)
                    return conversation.status && conversation.status.hasOwnerUpdates === 1;
                else if (conversation.toId === $rootScope.currentUid)
                    return conversation.status && conversation.status.hasReceiverUpdates === 1
                else
                    return 0;
            };

            $scope.getConversations = function() {
                var user = $rootScope.currentUser;
                if (user) {
                    $scope.conversations = [];
                    var ref = firebase.database().ref("conversations");
                    ref.orderByChild('fromId').startAt(user.uid).endAt(user.uid).on("value", function(snapshot) {
                        angular.forEach(snapshot.val(), function(f, key) {
                            if (f.lastMessage) {
                                var item = f;
                                item.key = key;
                                item.userProfile = $rootScope.listUsers[item.toId];;
                                Utils.checkAndAdd($scope.conversations, item);
                            }
                        });
                    });
                    ref.orderByChild('toId').startAt(user.uid).endAt(user.uid).on("value", function(snap) {
                        angular.forEach(snap.val(), function(f, key) {
                            if (f.lastMessage) {
                                var item = f;
                                item.key = key;
                                item.userProfile = $rootScope.listUsers[item.fromId];
                                Utils.checkAndAdd($scope.conversations, item);
                               
                            }
                        });
                    });
                }               
            };

            
            $scope.getConversations();
            $scope.cancel = function() {
                $scope.searchQuery = "";
                $scope.users = [];
            };
        }
    ]);
})();
