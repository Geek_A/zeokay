(function() {
    'use strict';
    zeokey.controller('SearchCtrl', ['$scope', '$rootScope', '$location', '$ionicPopup', '$ionicListDelegate', '$translate', '$filter', '$timeout', '$stateParams', 'PushServices', 'Utils', 'Popup',
        function($scope, $rootScope, $location, $ionicPopup, $ionicListDelegate, $translate, $filter, $timeout, $stateParams, PushServices, Utils, Popup) {
            /*
             * go to url
             */
            $scope.goTo = function(path) {
                $location.path(path);
            };

            $scope.saveNotification = function(message, userId) {
                var database = firebase.database();
                var newMsgKey = database.ref().child('notifications').push().key;
                var notifData = {
                    message: message,
                    action: "user/" + $rootScope.currentUser.uid,
                    fromUser: $rootScope.currentUser.uid,
                    user: userId,
                    updatedAt: new Date().getTime(),
                    read: 0,
                    notificationKey: newMsgKey
                };
                database.ref('notifications/' + newMsgKey).set(notifData);
                firebase.database().ref('users/' + userId).on('value', function(profile) {
                    var user = profile.val();
                    if (user.deviceId) {
                        var promise = PushServices.sendPush(user.deviceId, message, "user/" + $rootScope.currentUser.uid);
                        promise.then(function(datas) {
                            //console.log(datas);
                        });
                    }
                });
            };

            // An alert dialog
            $scope.showAlert = function(title, template) {
                 Utils.message(Popup.successIcon, template);
            };

            $scope.cancel = function() {
                $scope.searchQuery = "";
            };

            // need to be after declaration
            $scope.omitUsers = function(elm) {
                return (elm.email === $rootScope.currentUser.email) ||
                    $rootScope.listFriends.findIndex(function(element) {
                        return element.userProfile && element.userProfile.key === elm.key;
                    }) != -1;
            };

            $scope.showMail = function() {
                $scope.data = {};
                var myPopup = $ionicPopup.show({
                    template: '<input type="text" ng-model="data.email">',
                    title: $translate.instant("MAIL_ADD"),
                    subTitle: $translate.instant("ADD_EMAIL"),
                    scope: $scope,
                    buttons: [
                        { text: $translate.instant("CANCEL") },
                        {
                            text: $translate.instant("SEND"),
                            type: 'button-positive',
                            onTap: function(e) {
                                if ($scope.data.email) {
                                    return $scope.data.email;
                                }
                            }
                        }
                    ]
                });

                myPopup.then(function(res) {
                    if (res) {
                        if (res && window.plugins && window.plugins.emailComposer) {
                            window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                                    //console.log("Response -> " + result);
                                    //alert(result);
                                    $scope.showAlert($translate.instant("INVITATION_TITLE"), $translate.instant("INVITATION_CONTENT"));
                                },
                                $translate.instant("MAIL_SUBJECT"), // Subject
                                $translate.instant("MAIL_BODY"), // Body
                                [res], // To
                                null, // CC
                                null, // BCC
                                true, // isHTML
                                null, // Attachments
                                null); // Attachment Data
                        }
                    } else {
                        myPopup.close();
                    }
                });
            };

            $scope.addFavorite = function(key) {
                var confirmPopup = $ionicPopup.confirm({
                    title: $translate.instant("CONFIRM_ADD_FAVORITE_TITLE"),
                    template: $translate.instant("CONFIRM_ADD_FAVORITE_CONTENT")
                });
                confirmPopup.then(function(res) {
                    if (res) {
                        var convData = {
                            fromId: $rootScope.currentUser.uid,
                            toId: key,
                            updatedAt: new Date().getTime(),
                            createdAt: new Date().getTime()
                        };

                        var database = firebase.database();
                        var newConvKey = firebase.database().ref().child('conversations').push().key;
                        database.ref('conversations/' + newConvKey).set(convData);
                        var message = "Vous avez reçu une nouvelle demande d'ajout en favoris.";
                        $scope.saveNotification(message, key);
                        $ionicListDelegate.closeOptionButtons();
                         Utils.message(Popup.successIcon, $translate.instant("FAVORITE_SUCCESS_CONTENT"));
                    }
                });
            };

            $scope.showUser = function(userId) {
                $location.path("user/" + userId);
            };

        }
    ]);
})();