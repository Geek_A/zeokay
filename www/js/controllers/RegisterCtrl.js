(function() {
    'use strict';
    zeokey.controller('RegisterCtrl', ['$scope', '$firebaseAuth', '$ionicPopup', '$ionicModal', '$rootScope', '$translate', '$ionicLoading', '$location', 'Utils', 'Popup', 
        function($scope, $firebaseAuth, $ionicPopup, $ionicModal, $rootScope, $translate, $ionicLoading, $location, Utils, Popup) {

        $scope.showNavBar = true;
        
        $ionicModal.fromTemplateUrl('templates/cgu.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        $scope.close = function() {
            $scope.modal.hide();
        };

        $scope.showCgu = function() {
           // $scope.showAlert($translate.instant("CGU_TITLE"), $translate.instant("CGU_CONTENT"));
            $scope.modal.show();
        };

        $scope.createUser = function(user, form) {
            form.$submitted = true;
            if (form.$valid) {
                Utils.show();
                firebase.auth().createUserWithEmailAndPassword(user.email, user.password).then(function(userCreate) {
                    //console.log("create user success.", userCreate);
                    var deviceId = "";
                    if ($rootScope.deviceId) {
                        deviceId = $rootScope.deviceId;
                    }

                    firebase.database().ref('users/' + userCreate.uid).set({
                        email: user.email,
                        phone: user.phone,
                        username: user.username,
                        firstname: user.firstname,
                        name: user.name,
                        deviceId: deviceId,
                        createdAt: new Date().getTime(),
                        updatedAt: new Date().getTime(),
                        subscription: {
                            remaining_photos: 1,
                            formula_id: 'free',
                            remaining_proofs: 3,
                            subscribedAt: new Date().getTime()
                        }
                    });
                    localStorage.setItem('token', user.uid);
                    Utils.message(Popup.successIcon, $translate.instant("ACCOUNT_CREATE_CONTENT"));
                    //go to home page
                    $location.path("home");
                }, function(error) {
                    //console.log("error login", error);
                    showRegisterError(error);
                });

            }
        };

        var showRegisterError = function(error) {
            Utils.hide();
            switch (error.code) {
                case 'auth/email-already-in-use':
                    Utils.message(Popup.errorIcon, Popup.emailAlreadyExists);
                    break;
                default:
                    Utils.message(Popup.errorIcon, Popup.errorLogin);
                    break;
            }
        };

        /*
         * go to url
         */
        $scope.goTo = function(path) {
            $location.path(path);
        };
    }]);
})();