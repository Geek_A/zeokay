(function() {
    'use strict';
    zeokey.controller('LoginCtrl', ['$scope', '$firebaseAuth', '$location', '$ionicPopup', '$translate', '$ionicLoading', '$rootScope', '$cordovaOauth', '$timeout', 'Utils', 'Social', 'Popup',
        function($scope, $firebaseAuth, $location, $ionicPopup, $translate, $ionicLoading, $rootScope, $cordovaOauth, $timeout, Utils, Social, Popup) {
            window.addEventListener('native.keyboardshow', function() {
                $scope.showPadding = true;
            });

            window.addEventListener('native.keyboardhide', function() {
                $scope.showPadding = false;
            });


            $scope.googleplusLogin = function() {
                if (window.plugins && window.plugins.googleplus) {
                    Utils.show();
                    window.plugins.googleplus.login({
                            'webClientId': Social.googleWebClientId
                        },
                        function(obj) {
                            loginFirebase(firebase.auth.GoogleAuthProvider.credential(obj.idToken));
                        },
                        function(msg) {
                            //console.log("error2: " + msg);
                            window.plugins.googleplus.logout();
                            Utils.hide();
                        }
                    );
                } else
                    $scope.loginGoogle();
            };

            //This method is executed when the user press the "Login with facebook" button
            $scope.facebookSignIn = function() {
                if (window.plugins && facebookConnectPlugin) {
                    facebookConnectPlugin.getLoginStatus(function(success) {
                        if (success.status === 'connected') {
                            // The user is logged in and has authenticated your app, and response.authResponse supplies
                            // the user's ID, a valid access token, a signed request, and the time the access token
                            // and signed request each expire
                            var token = success.authResponse.accessToken;
                            loginFirebase(firebase.auth.FacebookAuthProvider.credential(token));

                        } else {
                            // If (success.status === 'not_authorized') the user is logged in to Facebook,
                            // but has not authenticated your app
                            // Else the person is not logged into Facebook,
                            // so we're not sure if they are logged into this app or not.

                            //console.log('getLoginStatus', success.status);

                            // Ask the permissions you need. You can learn more about
                            // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
                            facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
                        }
                    });
                } else
                    $scope.loginFacebook();
            };


            // This is the fail callback from the login method
            var fbLoginError = function(error) {
                Utils.message(Popup.errorIcon, Popup.errorLogin);
            };

            // This is the success callback from the login method
            var fbLoginSuccess = function(response) {
                if (!response.authResponse) {
                    fbLoginError("Cannot find the authResponse");
                    return;
                } else {
                    var token = response.authResponse.accessToken;
                    loginFirebase(firebase.auth.FacebookAuthProvider.credential(token));
                }
            };

            //Check if the Social Login used already has an account on the Firebase Database. If not, the user is asked to complete a form.
            var checkAndLoginAccount = function(firebaseUser) {
                var userId = firebase.auth().currentUser.uid;
                firebase.database().ref('users/' + firebaseUser.uid).once('value').then(function(accounts) {
                    if (accounts.exists()) {
                        if ($rootScope.deviceId)
                            firebase.database().ref('users/' + firebaseUser.uid).update({
                                deviceId: $rootScope.deviceId,
                                updatedAt: new Date().getTime()
                            });
                    } else {
                        //No account yet, proceed to completeAccount.
                        firebase.database().ref('users/' + firebaseUser.uid).set({
                            email: firebaseUser.email,
                            name: firebaseUser.displayName,
                            username: firebaseUser.displayName,
                            profile_picture: firebaseUser.photoURL,
                            deviceId: $rootScope.deviceId ? $rootScope.deviceId : '',
                            createdAt: new Date().getTime(),
                            updatedAt: new Date().getTime(),
                            subscription: {
                                remaining_photos: 1,
                                formula_id: 'free',
                                remaining_proofs: 3,
                                subscribedAt: new Date().getTime()
                            }
                        });
                        $scope.goTo('user');
                    }
                    Utils.hide();
                });
            };

            var loginFirebase = function(credential) {
                var provider = credential.provider;
                if (!firebase.auth().currentUser) {
                    firebase.auth().signInWithCredential(credential).then(function(user) {
                        checkAndLoginAccount(user);
                        $scope.goTo('home');
                        Utils.hide();
                    }).catch(function(error) {
                        showSocialLoginError(error);
                        // logout from social net
                        if (window.plugins.googleplus && provider === 'google.com')
                            window.plugins.googleplus.logout();
                        else if (facebookConnectPlugin && provider === 'facebook.com')
                            facebookConnectPlugin.logout();
                    });
                } else {
                    Utils.hide();
                }
            };

            //Using old cordova plugin
            $scope.loginFacebook = function() {
                $cordovaOauth.facebook(Social.facebookAppId, ["email"])
                    .then(function(result) {
                        Utils.show();
                        var credentials = firebase.auth.FacebookAuthProvider.credential(result.access_token);
                        return firebase.auth().signInWithCredential(credentials).then(function(firebaseUser) {
                            checkAndLoginAccount(firebaseUser);
                            Utils.hide();
                        }).catch(function(error) {
                            showSocialLoginError(error);
                        });
                    });
            };

            //Using old cordova plugin not supported anymore
            $scope.loginGoogle = function() {
                $cordovaOauth.google(Social.googleWebClientId, ["email"])
                    .then(function(result) {
                        Utils.show();
                        var credentials = firebase.auth.GoogleAuthProvider.credential(result.access_token);
                        return firebase.auth().signInWithCredential(credentials).then(function(firebaseUser) {
                            checkAndLoginAccount(firebaseUser);
                            Utils.hide();
                        }).catch(function(error) {
                            showSocialLoginError(error);
                        });
                    });
            };

            $scope.loginEmail = function(user, form) {
                if (form.$valid) {
                    Utils.show();
                    firebase.auth().signInWithEmailAndPassword(user.email, user.password).then(function(user) {
                        if ($rootScope.deviceId) {
                            firebase.database().ref('users/' + user.uid).update({
                                deviceId: $rootScope.deviceId
                            });
                        }
                        Utils.hide();
                        //console.log("login successful.", user.uid);
                        $location.path("home");
                    }, function(error) {
                        showFirebaseLoginError(error);
                    });
                }
            };

            /*
             * go to url
             */
            $scope.goTo = function(path) {
                $location.path(path);
            };

            //Shows the error popup message when using the Login with Firebase.
            var showFirebaseLoginError = function(errorCode) {
                Utils.hide();
                switch (errorCode.code) {
                    case 'auth/user-not-found':
                        Utils.message(Popup.errorIcon, Popup.emailNotFound);
                        break;
                    case 'auth/wrong-password':
                        Utils.message(Popup.errorIcon, Popup.wrongPassword);
                        break;
                    case 'auth/user-disabled':
                        Utils.message(Popup.errorIcon, Popup.accountDisabled);
                        break;
                    case 'auth/too-many-requests':
                        Utils.message(Popup.errorIcon, Popup.manyRequests);
                        break;
                    default:
                        Utils.message(Popup.errorIcon, Popup.errorLogin + ' ' + errorCode.message);
                        break;
                }
            };

            //Shows the error popup message when using the Social Login with Firebase.
            var showSocialLoginError = function(errorCode) {
                Utils.hide();
                switch (errorCode.code) {
                    case 'auth/account-exists-with-different-credential':
                        Utils.message(Popup.errorIcon, Popup.accountAlreadyExists);
                        break;
                    case 'auth/invalid-credential':
                        Utils.message(Popup.errorIcon, Popup.sessionExpired);
                        break;
                    case 'auth/operation-not-allowed':
                        Utils.message(Popup.errorIcon, Popup.serviceDisabled);
                        break;
                    case 'auth/user-disabled':
                        Utils.message(Popup.errorIcon, Popup.accountDisabled);
                        break;
                    case 'auth/user-not-found':
                        Utils.message(Popup.errorIcon, Popup.userNotFound);
                        break;
                    case 'auth/wrong-password':
                        Utils.message(Popup.errorIcon, Popup.wrongPassword);
                        break;
                    default:
                        Utils.message(Popup.errorIcon, Popup.errorLogin);
                        break;
                }
            };

        }
    ]);
})();