zeokey.factory('Utils', function($window, $ionicLoading, $timeout, $interval, Popup, $ionicPopup, $ionicModal, $cordovaCamera, $rootScope, $translate, $q, PushServices) {
    var promise;    
    var Utils = {
        show: function() {
            $ionicLoading.show({
                template: '<ion-spinner icon="ripple"></ion-spinner>'
            });
        },
        conneting: function() {
            $ionicLoading.show({
                template: '<p class="margin-left10 margin-right10 margin-top10">' + $translate.instant("RECONNEXION") + '</p><ion-spinner icon="lines"></ion-spinner>'
            });
        },
        hide: function() {
            $ionicLoading.hide();
        },
        progress: function(_scope) {
            var loadingPopup = $ionicLoading.show({
                templateUrl: 'templates/common/pre-progress.html',
                animation: 'fade-in',
                showBackdrop: true,
                hideOnStateChange: false,
                maxWidth: 300,
                showDelay: 0,
                scope: _scope
            });
            if (_scope.progressval === undefined)
                _scope.progressval = 0;
            promise = $interval(function() {
                if (_scope.progressval < 80)
                    _scope.progressval = _scope.progressval + 0.2;
                else if (_scope.progressval >= 99)
                    $interval.cancel(promise);
            }, 300, 1000);
            return loadingPopup;
        },
        toast: function (message) {
            if (window.plugins && window.plugins.toast){                
                return window.plugins.toast.showLongBottom($translate.instant(message));
            } else
                Utils.message('ion-ios-information-outline', message);
        },
        message: function (icon, message) {           
            $ionicLoading.show({
                template: '<div class="message-popup" onclick="hideMessage()"><h1><i class="icon ' + icon + '"></i></h1><p>' + $translate.instant(message) + '</p></div>',
                scope: this
            });
            promise = $timeout(function () {
                $ionicLoading.hide();
            }, Popup.delay);

            return promise;            
        },
        messagetxt: function(title, message) {
            var popup = $ionicLoading.show({
                template: '<div class="message-popup" onclick="hideMessage()"><h4>' + $translate.instant(title) + '</h4><hr/><p>' + $translate.instant(message) + '</p></div>',
                scope: this
            });
            promise = $timeout(function() {
                $ionicLoading.hide();
            }, Popup.delay);
            return promise;
        },
        confirm: function(icon, message, icon1, icon2) {
            var popup = $ionicPopup.confirm({
                cssClass: 'message-confirm',
                title: '<i class="icon ' + icon + '"></i>',
                template: $translate.instant(message),
                buttons: [{
                        text: '<i class="icon ' + icon1 + '"></i>',
                        type: 'button-confirm',
                        onTap: function(e) {
                            return false;
                        }
                    }, {
                        text: '<i class="icon ' + icon2 + '"></i>',
                        type: 'button-confirm',
                        onTap: function(e) {
                            return true;
                        }
                    }]
            });
            return popup;
        },
        confirmtxt: function(icon, message, text1, text2) {
            var popup = $ionicPopup.confirm({
                cssClass: 'message-confirm',
                title: '<i class="icon ' + icon + '"></i>',
                template: $translate.instant(message),
                buttons: [{
                        text: $translate.instant(text2),
                        type: 'button-confirm',
                        onTap: function(e) {
                            return false;
                        }
                    }, {
                        text: $translate.instant(text1),
                        type: 'button-confirm',
                        onTap: function(e) {
                            return true;
                        }
                    }]
            });
            return popup;
        },
        image: function(url) {
            var popup = $ionicPopup.confirm({
                cssClass: 'image-popup',
                template: '<div><img width="100%" ng-src="' + url + '"/></div>',
                buttons: [{
                        text: '<i class="icon ion-close"></i>',
                        type: 'button-confirm',
                        onTap: function(e) {
                            return false;
                        }
                    }]
            });
            return popup;
        },
        video0: function(video) {
            var max_height = window.innerHeight - 50;
            var url = (typeof video.then === 'function') ? video.ka : (video.url ? video.url : video);
            var popup = $ionicPopup.confirm({
                cssClass: 'image-popup',
                template: '<section class="video">' +
                        '<video width="100%" max-height="' + max_height + 'px" controls="controls" poster="' + video.thumb + '">' +
                        '<source src="' + url + '" type="video/mp4">' +
                        '</video>' +
                        '</section>',
                buttons: [{
                        text: '<i class="icon ion-close"></i>',
                        type: 'button-confirm',
                        onTap: function(e, video) {
                            return false;
                        }
                    }]
            });
            return popup;
        },
        video: function(video) {
            var url = (typeof video.then === 'function') ? video.ka : (video.url ? video.url : video);

            var modal = $ionicModal.fromTemplate('<ion-modal-view class="image-popup" style="background: rgba(0, 0, 0, 0.9);">' +
                    '<ion-content class="no-scroll" scroll=f"alse" overflow-scroll="true" has-bouncing="false">' +
                    '<button ng-click="modal.remove()" class="button button-icon button-positive button-close"><i class="icon ion-ios-close-outline"></i></button>' +
                    '<video class="centered" style="width: 100%; height: 100%;" poster="' + video.thumb + '" controls playsinline webkit-playsinline>' +
                    '<source src="' + url + '" type="video/mp4">' +
                    '</video>' +
                    '</ion-content>' +
                    '</ion-modal-view>',
                    {
                        animation: 'slide-in-up'
                    }
            );
            modal.show();

            return modal;
        },
        videoSimu: function(url, url2) {
            var height = window.innerHeight / 2 - 20;
            var modal = $ionicModal.fromTemplate('<ion-modal-view class="image-popup" style="background: rgba(0, 0, 0, 0.9);">' +
                    '<ion-content class="no-scroll" scroll=f"alse" overflow-scroll="true" has-bouncing="false" style="margin-top: 10px;">' +
                    '<button ng-click="modal.remove()" class="button button-icon button-close button-positive"><i class="icon ion-ios-close-outline"></i></button>' +
                    '<div class="centered">' +
                    '<video width="100%" height="' + height + 'px" controls webkit-playsinline playsinline><source src="' + url + '" type="video/mp4"></video>' +
                    '<video width="100%" height="' + height + 'px" controls webkit-playsinline playsinline=><source src="' + url2 + '" type="video/mp4"></video>' +
                    '</div>' +
                    '</ion-content>' +
                    '</ion-modal-view>',
                    {
                        animation: 'slide-in-up'
                    }
            );
            modal.show();

            return modal;
        },
        videoSimu0: function(url, url2) {
            var height = window.innerHeight / 2 - 50;
            var popup = $ionicPopup.confirm({
                cssClass: 'image-popup',
                template: '<div><video width="100%" height="' + height + 'px" controls webkit-playsinline playsinline><source src="' + url + '" type="video/mp4"></video></div>' +
                        '<div><video width="100%" height="' + height + 'px" controls webkit-playsinline playsinline=><source src="' + url2 + '" type="video/mp4"></video></div>',
                buttons: [{
                        text: '<i class="icon ion-close"></i>',
                        type: 'button-confirm',
                        onTap: function(e) {
                            return false;
                        }
                    }]
            });
            return popup;
        },
        generateFilename: function() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 100; i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text + ".png";
        },
        dataURItoBlob: function(dataURI) {
            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for (var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], {
                type: mimeString
            });
        },
        generateVideoTumbnail: function(video) {
            var defer = $q.defer();
            if (video && $window.PKVideoThumbnail) {
                //Utils.show();
                var options = {mode: 'array'};
                window.PKVideoThumbnail.createThumbnail(video.localURL, video.name, options).then(function(data) {
                    var metadata = {'contentType': 'image/jpeg'};
                    return firebase.storage().ref().child('video_pic/' + Utils.generateFilename()).put(data, metadata).then(function(snapshot) {
                        //File successfully uploaded to Firebase Storage.
                        defer.resolve(snapshot.metadata.downloadURLs[0]);
                    }).catch(function(error) {
                        //console.log('error' + error);
                        defer.reject(error);
                    });
                }).catch(function(err) {
                    defer.reject(err);
                });
            }
            return defer.promise;
        },
        getVideoPicture: function(imageSource) {

            //Set Camera options here.
            var options = {
                quality: 95,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: imageSource,
                encodingType: Camera.EncodingType.PNG,
                targetWidth: 614,
                targetHeight: 400,
                allowEdit: false,
                popoverOptions: CameraPopoverOptions,
                correctOrientation: false,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                //Create imageURI.
                var imgURI = "data:image/png;base64," + imageData;
                //Create Blob File from ImageURI.
                var file = Utils.dataURItoBlob(imgURI);
                //Create and set Meta Type to Firebase Storage Ref.
                var storageRef = firebase.storage().ref();
                var metadata = {
                    'contentType': file.type
                };
                Utils.show();
                var imageName = Utils.generateFilename();
                //Refer to images folder of Firebase Storage.
                storageRef.child('video_pic/' + imageName).put(file, metadata).then(function(snapshot) {
                    //File successfully uploaded to Firebase Storage.
                    var url = snapshot.metadata.downloadURLs[0];
                    // $scope.sendMessage('image', url);

                    $rootScope.$broadcast('videoImageUploaded', {
                        imageUrl: url,
                        imageName: imageName
                    });
                }).catch(function(error) {
                    //alert(error);
                    //Show Error.
                    Utils.message(Popup.errorIcon, Popup.uploadImageError);
                });
            }, function(err) {
                //User Cancelled.
                Utils.hide();
            });
        },
        getProfilePicture: function(imageSource) {

            //Set Camera options here.
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: imageSource,
                encodingType: Camera.EncodingType.PNG,
                targetWidth: 320,
                targetHeight: 320,
                allowEdit: true,
                popoverOptions: CameraPopoverOptions,
                correctOrientation: false,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                //Create imageURI.
                var imgURI = "data:image/png;base64," + imageData;
                //Create Blob File from ImageURI.
                var file = Utils.dataURItoBlob(imgURI);
                //Create and set Meta Type to Firebase Storage Ref.
                var storageRef = firebase.storage().ref();
                var metadata = {
                    'contentType': file.type
                };
                Utils.show();
                //Refer to images folder of Firebase Storage.
                storageRef.child('images/' + Utils.generateFilename()).put(file, metadata).then(function(snapshot) {
                    //File successfully uploaded to Firebase Storage.
                    var url = snapshot.metadata.downloadURLs[0];
                    // $scope.sendMessage('image', url);

                    $rootScope.$broadcast('profileImageUploaded', {
                        imageUrl: url
                    });
                }).catch(function(error) {
                    //alert(error);
                    //Show Error.
                    Utils.message(Popup.errorIcon, Popup.uploadImageError);
                });
            }, function(err) {
                //User Cancelled.
                Utils.hide();
            });
        },
        getPicture: function(imageSource) {

            //Set Camera options here.
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: imageSource,
                encodingType: Camera.EncodingType.PNG,
                targetWidth: 576,
                targetHeight: 576,
                allowEdit: true,
                popoverOptions: CameraPopoverOptions,
                correctOrientation: true,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                //Create imageURI.
                var imgURI = "data:image/png;base64," + imageData;
                //Create Blob File from ImageURI.
                var file = Utils.dataURItoBlob(imgURI);
                //Create and set Meta Type to Firebase Storage Ref.
                var storageRef = firebase.storage().ref();
                var metadata = {
                    'contentType': file.type
                };
                Utils.show();
                //Refer to images folder of Firebase Storage.
                storageRef.child('images/' + Utils.generateFilename()).put(file, metadata).then(function(snapshot) {
                    //File successfully uploaded to Firebase Storage.
                    var url = snapshot.metadata.downloadURLs[0];
                    // $scope.sendMessage('image', url);

                    $rootScope.$broadcast('imageUploaded', {
                        imageUrl: url
                    });
                }).catch(function(error) {
                    //console.log('error' + error);
                    //Show Error.
                    Utils.message(Popup.errorIcon, Popup.uploadImageError);
                });
            }, function(err) {
                //console.log('err' + err);
                //User Cancelled.
                Utils.hide();
            });
        },
        checkAndAdd: function(arr, item) {
            var found = arr.findIndex(function(el) {
                return el.key === item.key;
            });

            if (found != -1) {
                angular.extend(arr[found], item);
            } else
                arr.push(item);
        },
        checkAndRemove: function(arr, item) {
            var key = angular.isObject(item) ? item.key : item;
            var found = arr.findIndex(function(el) {
                return el.key === key;
            });

            if (found != -1)
                arr.splice(found, 1);
        },        
        clearUserDate: function() {
            $rootScope.videos = [];
            $rootScope.receiptvideos = [];
            $rootScope.progress = 0;
            $rootScope.currentUid = undefined;
            $rootScope.currentProfile = undefined;
            $rootScope.currentFormula = undefined;
            $rootScope.fromMeConversations = [];
            $rootScope.toMeConversations = [];
            $rootScope.listFriends = [];
            $rootScope.notifications = [];
            $rootScope.sources = [];
        },
        deleteUser: function(key) {
            var confirmPopup = $ionicPopup.confirm({
                title: $translate.instant("CONFIRM_DELETE_USER"),
                template: $translate.instant("CONFIRM_DELETE_USER_CONTENT")
            });
            confirmPopup.then(function(res) {
                if (res) {
                    var ref = firebase.database().ref("conversations/" + key);
                    ref.remove().then(function() {
                        Utils.messagetxt($translate.instant("FAVORITE_SUCCESS_TITLE"), $translate.instant("FAVORITE_SUCCESS_REMOVE"));
                    });

                }
            });
        }

    };
    hideMessage = function() {
        $timeout.cancel(promise);
        $ionicLoading.hide();
    };

    return Utils;
});