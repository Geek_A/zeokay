(function() {
    'use strict';
    zeokey.controller('ProofCtrl', ['$scope', '$rootScope', '$state', '$location', '$stateParams', '$translate', '$ionicModal', '$ionicScrollDelegate', '$ionicPopup', '$ionicHistory', '$ionicLoading', '$ionicPlatform', '$window', '$sce', 'PushServices', 'Popup', 'Utils', '$timeout', '$interval',
        function($scope, $rootScope, $state, $location, $stateParams, $translate, $ionicModal, $ionicScrollDelegate, $ionicPopup, $ionicHistory, $ionicLoading, $ionicPlatform, $window, $sce, PushServices, Popup, Utils, $timeout, $interval) {
            $scope.imgMenu = "img/icons/send-btn.png";
            $scope.imgMenuRed = "img/icons/send-btn.png";
            $scope.proofThumb = "img/proof_covert.png";
            $scope.today = new Date();
            var firstThumbImage = new Image();
            firstThumbImage.src = $scope.proofThumb;
            var videoUpladCompleted = false;
            var videoUploadFailed = false;
            var saveClicked = false;

            try {
                $scope.video = $stateParams.slug ? JSON.parse($stateParams.slug) : {};
            } catch (error) {
                $scope.video = {};
            }
            $scope.progressval = 0;


            firebase.database().ref('sampleCategories').once('value').then(function(snapshot) {
                //var values = snapshot.val() ? Object.keys(snapshot.val()).map(function(key) { return snapshot.val()[key]; }) : [];
                $scope.sampleCategories = snapshot.val();
            });

            $ionicModal.fromTemplateUrl('search.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
            });

            // Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function() {
                $scope.modal.remove();
            });

            // need to be after declaration
            $scope.omitUsers = function(elm) {
                return (elm.email === $rootScope.currentUser.email);
            };

            $scope.searchUser = function() {
                $scope.modal.show();
            };

            $scope.showMail = function() {
                $scope.data = {};
                var myPopup = $ionicPopup.show({
                    template: '<input type="text" ng-model="data.email">',
                    title: $translate.instant("MAIL_ADD"),
                    subTitle: $translate.instant("ADD_EMAIL"),
                    scope: $scope,
                    buttons: [
                        { text: $translate.instant("CANCEL") },
                        {
                            text: $translate.instant("SEND"),
                            type: 'button-positive',
                            onTap: function(e) {
                                if ($scope.data.email) {
                                    return $scope.data.email;
                                }
                            }
                        }
                    ]
                });

                myPopup.then(function(res) {
                    if (res) {
                        if (res && window.plugins && window.plugins.emailComposer) {
                            window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                                    //console.log("Response -> " + result);
                                    //alert(result);
                                    Utils.messagetxt($translate.instant("INVITATION_TITLE"), $translate.instant("INVITATION_CONTENT"));
                                },
                                $translate.instant("MAIL_SUBJECT"), // Subject
                                $translate.instant("MAIL_BODY"), // Body
                                [res], // To
                                null, // CC
                                null, // BCC
                                true, // isHTML
                                null, // Attachments
                                null); // Attachment Data
                        }
                    } else {
                        myPopup.close();
                    }
                });
            };

            $scope.cancel = function() {
                $scope.searchQuery = undefined;
                $scope.users = [];
            };

            $scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
            };

            $scope.actionMenu = function() {
                if ($scope.sendToUser && $scope.video.title && $scope.video.category && $scope.video.sub_category) {
                    saveClicked = true;
                    if (videoUploadFailed) {
                        uploadVideoToFirebase($scope.video);
                        Utils.progress($scope);
                    } else if (!videoUpladCompleted) {
                        Utils.progress($scope);
                    } else {
                        Utils.show();
                        createProofMetadata($scope.video);
                    }
                } else {
                    Utils.message(Popup.errorIcon, $translate.instant("PROOF_CONTENT"));
                }
            };

            var createProofMetadata = function (video) {
                var database = firebase.database();
                var user = $rootScope.currentUser;
                if (user) {
                    var userId = user.uid;
                    var archiveAt = new Date();
                    var archiveTime = 0;
                    if ($rootScope.currentFormula) {
                        archiveAt.setDate(archiveAt.getDate() + $rootScope.currentFormula.storage_duration);
                        archiveTime = archiveAt.getTime();
                    }

                    try {
                        
                        var videoData = {
                            name: video.name,
                            createdAt: firebase.database.ServerValue.TIMESTAMP,
                            archiveAt: archiveTime,
                            ownerUid: userId,
                            category: video.category.name,
                            subcategory: video.sub_category.name,
                            send_user: user.email,
                            thumb: $scope.proofThumb,
                            owner_name: $rootScope.currentProfile.name,
                            title: video.title,
                            receipt_user: $scope.sendToUser.email,
                            receipt_name: $scope.sendToUser.name,
                            receiptUid: $scope.sendToUser.key
                        };
                        var newVideoKey = database.ref().child('videos').push().key;
                        database.ref('videos/' + newVideoKey).set(videoData);

                        var msgData = {
                            videoId: newVideoKey,
                            type: "video",
                            thumb: $scope.proofThumb,
                            fromId: userId,
                            url: video.downloadURL,
                            updatedAt: firebase.database.ServerValue.TIMESTAMP
                        };
                        var newProofMsgKey = database.ref().child('proof_info').push().key;
                        database.ref('proof_info/' + newProofMsgKey).set(msgData).then(function(){
                            //kept now due to firebase function delay
                            $rootScope.currentProfile.subscription.remaining_proofs = Math.max($rootScope.currentProfile.subscription.remaining_proofs - 1, 0);
                            $rootScope.progress = $rootScope.currentProfile.subscription.remaining_proofs / $rootScope.currentFormula.nbr_proof;
                            // end progress

                            if ($scope.sendToUser.deviceId) {
                                var message = $translate.instant('PROOF_RECEIVED', { from: $scope.sendToUser.name });
                                var promise = PushServices.sendPush($scope.sendToUser.deviceId, message, "proofdetail/" + newVideoKey);
                                promise.then(function (datas) {
                                    //console.log(datas);
                                });
                            }

                            var notifMsgKey = database.ref().child('notifications').push().key;
                            var notifData = {
                                message: $translate.instant('PROOF_RECEIVED', { from: $scope.sendToUser.name }),
                                action: "proofdetail/" + newVideoKey,
                                fromUser: userId,
                                user: $scope.sendToUser.key,
                                updatedAt: firebase.database.ServerValue.TIMESTAMP,
                                read: 0,
                                notificationKey: notifMsgKey
                            };
                            database.ref('notifications/' + notifMsgKey).set(notifData);

                            Utils.message(Popup.successIcon, $translate.instant("PROOF_CREATE_CONTENT")).finally(function () {
                                if ($rootScope.currentProfile.subscription.remaining_proofs === 1) {
                                    var newMsgKey = database.ref().child('notifications').push().key;
                                    var notifData = {
                                        message: $translate.instant("PROOF_ONLY_ONE"),
                                        action: "subscription",
                                        fromUser: userId,
                                        user: userId,
                                        updatedAt: firebase.database.ServerValue.TIMESTAMP,
                                        read: 0,
                                        notificationKey: newMsgKey
                                    };
                                    database.ref('notifications/' + newMsgKey).set(notifData);
                                    var confirmPopup = Utils.confirmtxt("ion-information-circled", "PROOF_ONLY_ONE", "CHANGE", "OK_VIDEO_1");
                                    confirmPopup.then(function (res) {
                                        if (res)
                                            $state.go("formulas");
                                        else
                                            $state.go("home");
                                    });
                                } else {
                                    $state.go("home");
                                }
                            });
                        });
                    } catch (e) {
                        console.error(e);
                        Utils.message(Popup.errorIcon, "ERROR_INPUT");
                    }
                } 
            };

            var uploadVideoToFirebase = function(video) {
                if (window.cordova && window.cordova.plugins)
                    $window.resolveLocalFileSystemURL(video.localURL, function(fileEntry) {
                        fileEntry.file(function(file) {
                            var reader = new FileReader();
                            reader.onloadend = function() {
                                //console.log("Successful file write: " + this.result);
                                var metadata = {
                                    contentType: 'video/mp4',
                                    maxOperationRetryTime: 100000,
                                    maxUploadRetryTime: 100000
                                };

                                var blob = new Blob([new Uint8Array(this.result)], metadata);
                                var uploadTask = firebase.storage().ref().child('videos/' + video.name).put(blob);

                                if (uploadTask) {
                                    uploadTask.on('state_changed', function(snapshot) {
                                        // Observe state change events such as progress, pause, and resume
                                        // See below for more detail
                                        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                                        $scope.progressval = progress;
                                        //console.log('Upload is ' + progress + '% done');
                                        $scope.$apply();

                                    }, onErrorReadFile, function() {
                                        // Handle successful uploads on complete
                                        $scope.video.downloadURL = uploadTask.snapshot.downloadURL;
                                        videoUpladCompleted = true;
                                        videoUploadFailed = false;
                                        if (saveClicked)
                                            createProofMetadata($scope.video);
                                    });
                                } else {
                                    Utils.hide();
                                }
                            };
                            reader.readAsArrayBuffer(file);

                        }, onErrorReadFile);
                    }, onErrorReadFile);
            };

            var onErrorReadFile = function(error) {
                // Handle unsuccessful uploads
                videoUploadFailed = true;
                //Utils.hide();
                Utils.message(Popup.errorIcon, "Error uploading: " + error);
                //console.log("Error uploading: " + error);
            }

            var onErrorFb = function(error) {
                
                //Utils.hide();
                Utils.message(Popup.errorIcon, "Error uploading: " + error);
                //console.log("Error uploading: " + error);
            }

            $scope.showSetting = function() {
                $location.path("/setting");
            };

            $scope.selectUser = function(user) {
                $scope.sendToUser = user;
                $scope.modal.hide();
            };

            $scope.takePicture = function() {
                //Show loading.
                var popup = Utils.confirm("ion-link", "SELECT_PROOF_PHOTO", "ion-images", "ion-camera");
                popup.then(function(isCamera) {
                    var imageSource;
                    if (isCamera) {
                        imageSource = Camera.PictureSourceType.CAMERA;
                    } else {
                        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
                    }
                    //Show loading.
                    Utils.getVideoPicture(imageSource);
                });
            };
            //Broadcast from our Utils.getPicture function that tells us that the image selected has been uploaded.
            $scope.$on('videoImageUploaded', function(event, args) {
                //Proceed with sending of image message.
                //console.log(args.imageUrl);
                
                $stateParams.capture = args.imageName;
                firstThumbImage.src = args.imageUrl;
                firstThumbImage.onload = function() {
                    $scope.proofThumb = args.imageUrl;
                };
                Utils.hide();
            });

            $scope.$on('$ionicView.leave', function() {
                // Anything you can think of

            });

            $scope.generateVideoTumbnail = function() { //$scope.$on('$ionicView.afterEnter', function() {
                Utils.generateVideoTumbnail($scope.video).then(function(url) {
                    //File successfully uploaded to Firebase Storage.                    
                    $scope.proofThumb = url;
                    firstThumbImage.src = url;
                }).catch(function(error) {
                    //console.log('error' + error);
                    //Show Error.
                    Utils.message(Popup.errorIcon, Popup.uploadImageError);
                });
            };

            $scope.generateVideoTumbnail();
            uploadVideoToFirebase($scope.video);
            //});

            /*
             * go to url we prevent leaving this view
             */
            $scope.goTo = function(path) {
                Utils.confirmtxt(Popup.errorIcon, "NEW_PROOF_LEAVE", "OUI", "NON").then(function(res) {
                    if (res) {                        
                        $state.go(path);
                    }
                });
            };
        
            $scope.hScroll = function(element) {
                var srcElement = event.srcElement;
                var element = event.srcElement.parentNode.childNodes[1];
                ionic.requestAnimationFrame(function() {
                    if ($ionicScrollDelegate.getScrollPosition()) {
                        if ($ionicScrollDelegate.getScrollPosition().top > 120) {
                            element.classList.remove("white");
                            srcElement.style.backgroundSize = '0%';
                        } else {
                            element.classList.add("white");
                            srcElement.style.backgroundSize = '100%';
                        }
                        if ($ionicScrollDelegate.getScrollPosition().top < 0) {
                            var sheight = 185 - $ionicScrollDelegate.getScrollPosition().top;
                            //(original height / original width) x new width = new height
                            var imgRealsize = (firstThumbImage.height / firstThumbImage.width) * window.innerWidth;
                            if (imgRealsize < sheight) {
                                srcElement.style.backgroundSize = 'auto ' + sheight + 'px';
                            } else
                                srcElement.style.backgroundSize = '100%';
                        }
                    }
                });
            };

            $scope.getMinHeight = function() {
                return { 'min-height': window.innerHeight - 40 + 'px' };
            };
        }
    ]);
})();