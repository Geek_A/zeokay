(function() {
    'use strict';
    zeokey.service('DataService', function($ionicLoading, $timeout, $ionicPopup, $cordovaCamera, $rootScope) {
        this.getListContacts = function(user) {
            var ref = firebase.database().ref("conversations");
            ref.orderByChild('fromId').startAt(user.uid).endAt(user.uid).on("value", function(snapshot) {
                angular.forEach(snapshot.val(), function(f, key) {
                    var item = f;
                    item.key = key;
                    item.conversation = item.key;
                    item.uid = item.toId;
                    item.userProfile = $rootScope.listUsers[item.toId];
                    Utils.checkAndAdd($rootScope.listFriends, item);
                });
            });
            ref.orderByChild('toId').startAt(user.uid).endAt(user.uid).on("value", function(snap) {
                angular.forEach(snap.val(), function(f, key) {
                    var item = f;
                    item.key = key;
                    item.conversation = item.key;
                    item.uid = item.toId;
                    item.userProfile = $rootScope.listUsers[item.fromId];
                    Utils.checkAndAdd($rootScope.listFriends, item);
                });
            });

            /*
             Watch videos changes
             */
            firebase.database().ref("conversations").on('child_removed', function(change) {
                Utils.checkAndRemove($rootScope.listFriends, change.key);
            });
        };
    }).controller('ContactCtrl', ['$scope', '$state', '$rootScope', '$location', '$translate', '$ionicPopup', '$interval', '$filter', '$timeout', '$ionicLoading', '$ionicHistory', 'DataService', 'Utils', 'Popup',
        function($scope, $state, $rootScope, $location, $translate, $ionicPopup, $interval, $filter, $timeout, $ionicLoading, $ionicHistory, DataService, Utils, Popup) {
            /*
             * go to url
             */
            $scope.goTo = function(path, params) {
                $state.go(path, params);
            };
            $scope.priviousUrl = "home";
            $scope.state = $ionicHistory.currentStateName();
            if ($ionicHistory.backView()) {
                $scope.priviousUrl = $ionicHistory.backView().url;
            }   
            
            $scope.deleteUser = function(key) {
                var confirmPopup = $ionicPopup.confirm({
                    title: $translate.instant("CONFIRM_DELETE_USER"),
                    template: $translate.instant("CONFIRM_DELETE_USER_CONTENT")
                });
                confirmPopup.then(function(res) {
                    if (res) {
                        var ref = firebase.database().ref("conversations/" + key);
                        ref.remove().then(function(){
                            Utils.message(Popup.successIcon, $translate.instant("FAVORITE_SUCCESS_REMOVE"));  
                        });
                        
                    }
                });
            };
            $scope.showUser = function(userId) {
                $location.path("user/" + userId);
            };
            $scope.showMail = function() {
                $scope.data = {};
                var myPopup = $ionicPopup.show({
                    template: '<input type="text" ng-model="data.email">',
                    title: $translate.instant("MAIL_ADD"),
                    subTitle: $translate.instant("ADD_EMAIL"),
                    scope: $scope,
                    buttons: [{
                            text: $translate.instant("CANCEL")
                        }, {
                            text: $translate.instant("SEND"),
                            type: 'button-positive',
                            onTap: function(e) {
                                if ($scope.data.email) {
                                    return $scope.data.email;
                                }
                            }
                        }]
                });
                myPopup.then(function(res) {
                    if (res) {
                        if (res && window.plugins && window.plugins.emailComposer) {
                            window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                                //alert(result);
                                $scope.showAlert($translate.instant("INVITATION_TITLE"), $translate.instant("INVITATION_CONTENT"));
                            }, $translate.instant("MAIL_SUBJECT"), // Subject
                                    $translate.instant("MAIL_BODY"), // Body
                                    [res], // To
                                    null, // CC
                                    null, // BCC
                                    true, // isHTML
                                    null, // Attachments
                                    null); // Attachment Data
                        }
                    } else {
                        myPopup.close();
                    }
                });
            };
            $scope.cancel = function() {
                $scope.searchQuery = "";
                $scope.users = [];
            };
            // An alert dialog
            $scope.showAlert = function (title, template) {
                Utils.message(Popup.successIcon, template);
            };
         
        }
    ]);
})();