(function () {
    'use strict';
    zeokey.controller('FormulaCtrl', ['$scope', '$rootScope', '$location', '$ionicSlideBoxDelegate', '$ionicHistory',
        function ($scope, $rootScope, $location, $ionicSlideBoxDelegate, $ionicHistory) {
            $scope.priviousUrl = "subscription";

            if ($ionicHistory.backView()) {
                $scope.priviousUrl = $ionicHistory.backView().url;
            }
            $scope.goTo = function (path) {
                $location.path(path);
            };
           
            $scope.slideHasChanged = function (index) {
                /* This order is importent for formulas selection */
                $rootScope.currentIndex = index;
            };   
            $rootScope.currentIndex = 0;
        }
    ]);
})();
