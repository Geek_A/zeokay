(function () {
    'use strict';
    zeokey.controller('HelpCtrl', ['$scope', '$rootScope', '$location', '$ionicHistory',
        function ($scope, $rootScope, $location, $ionicHistory) {
            $scope.goTo = function (path) {
                $location.path(path);
            };
			
			$scope.priviousUrl = "setting";
            if ($ionicHistory.backView()) {
               $scope.priviousUrl = $ionicHistory.backView().url;
            }

            firebase.auth().onAuthStateChanged(function (user) {
                if (!user) {
                    $location.path("/login");
                }
            });
        }
    ]);
})();
