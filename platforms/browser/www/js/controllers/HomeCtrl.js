(function() {
    'use strict';
    zeokey.directive('ngClick', function ($timeout) {
        var delay = 500;   // min milliseconds between clicks

        return {
            restrict: 'A',
            priority: -1,   // cause out postLink function to execute before native `ngClick`'s
                            // ensuring that we can stop the propagation of the 'click' event
                            // before it reaches `ngClick`'s listener
            link: function (scope, elem) {
                var disabled = false;

                function onClick(evt) {
                    if (disabled) {
                        evt.preventDefault();
                        evt.stopImmediatePropagation();
                    } else {
                        disabled = true;
                        $timeout(function () { disabled = false; }, delay, false);
                    }
                }

                scope.$on('$destroy', function () { elem.off('click', onClick); });
                elem.on('click', onClick);
            }
        };
    }).directive('spinnerOnLoad', function() {
            return {
                restrict: 'A',
                transclude: true,
                scope: {
                    ngStyle: '@'
                },
                link: function($scope, element, attr, preImgController) {
                    var regexp = /url\s?\((.+)\)/;
                    var img_src = regexp.exec(attr.ngStyle)[1];
                    var bg = img_src.replace(/(['"])/g, '');
                    if (bg) {
                        var img = new Image();
                        img.src = bg;
                        img.onload = function() {
                            // do something, maybe:
                            hideSpinner();
                        };
                    }

                    var hideSpinner = function() {
                        // Think i have to use apply because this function is not called from this controller ($scope)
                        $scope.$apply(function() {
                            $scope.loaded = true;
                        });
                    };
                },
                templateUrl: 'templates/common/pre-img.html'
            };
        })
        .filter('orderObjectByDate', function() {
            return function(input) {
                if (!angular.isObject(input))
                    return input;

                var array = [];
                for (var objectKey in input) {
                    array.push(input[objectKey]);
                }

                array.sort(function(a, b) {
                    a = parseInt(a['updatedAt'] ? a['updatedAt'] : a['createdAt']);
                    b = parseInt(b['updatedAt'] ? b['updatedAt'] : b['createdAt']);
                    return b - a;
                });
                return array;
            }
        }).controller('HomeCtrl', ['$scope', '$state', '$rootScope', '$ionicSideMenuDelegate', '$sce', '$cordovaCapture', '$ionicHistory', '$ionicPopup', '$ionicLoading', '$ionicPlatform',
            '$timeout', 'Utils', 'video', '$translate', '$filter', '$location', '$window', '$ionicModal', '$interval', '$cordovaFileTransfer', '$cordovaFile',
            function($scope, $state, $rootScope, $ionicSideMenuDelegate, $sce, $cordovaCapture, $ionicHistory, $ionicPopup, $ionicLoading, $ionicPlatform, $timeout, Utils, video, $translate,
                $filter, $location, $window, $ionicModal, $interval, $cordovaFileTransfer, $cordovaFile) {
                // An alert dialog
                $rootScope.isLogged = false;
                $rootScope.videosLimit = 20;
                $rootScope.rvideosLimit = 20;

                $ionicPlatform.registerBackButtonAction(function (event) {
                  if ($ionicHistory.currentStateName() === 'home' 
                    || $ionicHistory.currentStateName() === 'proof' 
                    || $ionicHistory.currentStateName() === 'recap'){
                    event.preventDefault();
                  } else {
                    $ionicHistory.goBack();
                  }
                }, 100);

                $scope.showSend = function() {
                    $rootScope.showSendVideo = true;
                };
                $scope.showRecieved = function() {
                    $rootScope.showSendVideo = false;
                };
                if ($rootScope.showSendVideo || $rootScope.showSendVideo === undefined)
                    $scope.showSend();
                else
                    $scope.showRecieved();

                var canGo = false;
                $timeout(function() {
                    canGo = true;
                }, 1200);
                $scope.hasvideos = false;

                window.addEventListener('native.keyboardshow', function() {
                    document.body.classList.add('keyboard-open');
                });
                $rootScope.hideKeyboard = function(path, params) {
                    if(window.cordova && window.cordova.plugins)
                        cordova.plugins.Keyboard.close();
                    document.body.classList.remove('keyboard-open');
                };
                $scope.$on('$ionicView.beforeEnter', function() {
                    $rootScope.hideKeyboard();
                });

                $scope.trustSrc = function(src) {
                    return $sce.trustAsResourceUrl(src);
                };

                $scope.goTo = function(path, params) {
                    if (canGo)
                        $state.go(path, params);
                };

                $scope.validate = function(key) {
                    firebase.database().ref('videos/' + key).update({
                        validate: 1
                    });
                };

                $scope.refuse = function(key) {
                    $scope.data = {};
                    var myPopup = $ionicPopup.show({
                        template: '<textarea rows="4" cols="50" ng-model="data.comment"></textarea>',
                        title: $translate.instant("CANCEL_TITLE"),
                        subTitle: $translate.instant("CANCEL_COMMENT"),
                        scope: $scope,
                        buttons: [
                            { text: $translate.instant("CANCEL") },
                            {
                                text: $translate.instant("SEND"),
                                type: 'button-positive',
                                onTap: function(e) {
                                    if (!$scope.data.comment) {
                                        $scope.data.comment = "sans commentaire";
                                    }
                                    firebase.database().ref('videos/' + key).update({
                                        validate: 0,
                                        comment: $scope.data.comment
                                    });
                                }
                            }
                        ]
                    });
                };


                function makeid() {
                    var text = '';
                    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                    for (var i = 0; i < 5; i++) {
                        text += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    return text;
                };
                $scope.goToPublicProfil = function() {
                    $scope.goTo('user', { slug: $rootScope.currentUid });
                };

                $scope.showSetting = function() {
                    $location.path("/setting");
                };

                $scope.openVideo = function(url) {
                    //console.log("openLink", url);
                    $rootScope.link = url;
                    $ionicModal.fromTemplateUrl('templates/play-video.html', {
                        scope: $scope,
                        animation: 'slide-in-up'
                    }).then(function(modal) {
                        $scope.modal = modal;
                        $scope.modal.show();
                    });
                };

                $scope.open = function(e) {
                    var videoElements = angular.element(e.srcElement);
                    videoElements[0].play();
                    if (videoElements[0].webkitRequestFullScreen) {
                        videoElements[0].webkitRequestFullScreen();
                    }
                };
                $rootScope.deleteVideo = function(video) {
                    var confirmPopup = $ionicPopup.confirm({
                        title: $translate.instant("CONFIRM_DELETE_TITLE"),
                        template: $translate.instant("CONFIRM_DELETE_CONTENT")
                    });
                    confirmPopup.then(function(res) {
                        if (res) {
                            if ($rootScope.currentUid === video.ownerUid) {
                                firebase.database().ref('videos/' + video.key).update({
                                    owner_delete: 1,
                                    status: {}
                                });
                            } else {
                                firebase.database().ref('videos/' + video.key).update({
                                    recieve_delete: 1,
                                    status: {}
                                });
                            }
                        }
                    });

                    return confirmPopup;
                };

                $scope.saveVideo = function(videoName) {
                    //firebase.auth().onAuthStateChanged(function(user) {
                    var user = $rootScope.currentUser;
                    if (user) {
                        var userId = user.uid;
                        var videoData = {
                            name: videoName,
                            uid: userId,
                            title: "video1",
                            receiptid: userId
                        };
                        var database = firebase.database();
                        var newVideoKey = firebase.database().ref().child('videos').push().key;
                        database.ref('videos/' + newVideoKey).set(videoData);
                        $scope.getVideos();
                        $scope.loadingPopup.close();
                    } else {
                        $location.path("/login");
                    }
                    // });
                };

                $rootScope.actionMenu = function() {

                    if ($rootScope.progress <= 0) {
                        var confirmPopup = Utils.confirmtxt("ion-information-circled",
                            "Vous avez épuisé le nombre de preuve de votre abonnement. Voulez vous acheter des preuves supplémentaires ?",
                            "BUY", "CANCEL");
                        confirmPopup.then(function(res) {
                            if (res) {
                                $location.path("unitbuy");
                            } else {
                                //$location.path("subscription");
                            }
                        });
                    } else {
                        var message = $translate.instant('POPUP_VIDEO_1', { duration: $rootScope.currentFormula.capture_duration });
                        var confirmPopup = Utils.confirmtxt("ion-information-circled", message, 'OK_VIDEO_1', 'CANCEL_VIDEO_1');
                        confirmPopup.then(function(res) {
                            canGo = false;
                            // $rootScope.progress = ($rootScope.progress <= 0.1) ? 1 : $rootScope.progress - 0.1;
                            if (res) {
                                $location.path('home');
                                var duration = $rootScope.currentFormula.capture_duration ? 60 * $rootScope.currentFormula.capture_duration : 60;
                                var options = { limit: 1, duration: duration };
                                $cordovaCapture.captureVideo(options).then(function(videoData) {
                                    // wait camera to close then open
                                    canGo = true;
                                    $state.go("proof", { slug: JSON.stringify(videoData[0]), capture: "proof_covert.png" });
                                }, function(err) {
                                    canGo = true;
                                });
                            } else {
                                canGo = true;
                                $location.path("subscription");
                            }
                        });
                    }

                };

                $rootScope.onLongPress = function() {
                    var userId = $rootScope.currentUser.uid;
                    $location.path('user/' + userId);
                };

                $scope.toggleLeft = function() {
                    $ionicSideMenuDelegate.toggleLeft();
                };

                $rootScope.countUnreadProof = function() {
                    return $filter('filter')($scope.videos, { status: { hasOwnerUpdates: 1 }, owner_delete: 0 }).length +
                        $filter('filter')($scope.receiptvideos, { status: { hasReceiverUpdates: 1 }, recieve_delete: 0 }).length;
                };

                $rootScope.countUnreadMessages = function() {
                    var from = $filter('filter')($rootScope.fromMeConversations, { status: { hasOwnerUpdates: 1 } });
                    var to = $filter('filter')($rootScope.toMeConversations, { status: { hasReceiverUpdates: 1 } });
                    return (from ? from.length : 0) + (to ? to.length : 0);
                };

                 $rootScope.countUnreadNotifs= function() {
                    var unreadNotifs = $filter('filter')($rootScope.notifications, { read: 0 });
                    return (unreadNotifs ? unreadNotifs.length : 0);
                };

                $rootScope.onScroll = function(element) {
                    var srcElement = event.srcElement;
                    var element = event.srcElement.parentNode.childNodes[1];
                    ionic.requestAnimationFrame(function() {
                        if ($ionicScrollDelegate.getScrollPosition()) {
                            if ($ionicScrollDelegate.getScrollPosition().top > 120) {
                                element.classList.remove("white");                            
                            } else {
                                element.classList.add("white");
                                
                            }                                            
                        }
                    });
                };

            }
        ]);
})();