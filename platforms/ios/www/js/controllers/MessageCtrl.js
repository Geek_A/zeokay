(function() {
    'use strict';
    zeokey.controller('MessageCtrl', ['$scope', '$rootScope', '$location', '$ionicPopup', '$translate', '$stateParams', '$timeout', '$ionicScrollDelegate', '$ionicHistory', 'PushServices', 'Utils',
        function($scope, $rootScope, $location, $ionicPopup, $translate, $stateParams, $timeout, $ionicScrollDelegate, $ionicHistory, PushServices, Utils) {
            $scope.limit = 10;

            $scope.goTo = function(path) {
                firebase.database().ref('messages').off();
                $location.path(path);
            };
            $ionicScrollDelegate.scrollBottom();
            $scope.priviousUrl = "inbox";
            if ($ionicHistory.backView()) {
                $scope.priviousUrl = $ionicHistory.backView().url;
            }

            $scope.getMessages = function() {

                var conversationId = $stateParams.slug;
                
                $scope.messages = [];
                firebase.database().ref('conversations/' + conversationId).once('value').then(function(data) {
                    $scope.conversation = data.val();
                    var uidFriend = $scope.conversation.fromId;
                    if ($scope.conversation.fromId === $rootScope.currentUser.uid) {
                        uidFriend = $scope.conversation.toId;
                    }
                    return uidFriend;
                }).then(function(uidFriend) {

                    return firebase.database().ref('users/' + uidFriend).once('value').then(function(data) {
                        $scope.friend = data.val();
                        $scope.friend.uid = data.key;
                    });

                }).then(function() {

                    var ref = firebase.database().ref("messages");
                    ref.orderByChild("conversation").startAt(conversationId).endAt(conversationId).on("value", function(snapshot) {
                        var user = $rootScope.currentUser;
                        $timeout(function() {
                            if(user) {
                                $scope.messages = snapshot.val() ? Object.keys(snapshot.val()).map(function(key) { return snapshot.val()[key]; }) : [];
                                //update conversation status
                                if ($scope.conversation.fromId === user.uid)
                                    firebase.database().ref('conversations/' + conversationId + '/status').update({
                                        hasOwnerUpdates: 0
                                    });
                                else
                                    firebase.database().ref('conversations/' + conversationId + '/status').update({
                                        hasReceiverUpdates: 0
                                    });

                                angular.forEach($rootScope.notifications, function(notif, key) {
                                    //console.log(notif.user + ': ' + user.uid  + ' - ' + notif.action + ' : ' +  $location.path());
                                    if(notif.user && notif.action && notif.user === user.uid && '/' + notif.action ===  $location.path())
                                        firebase.database().ref('notifications/' + notif.key + '/read').set(1);
                                });
                            }

                            $timeout(function() {
                                $ionicScrollDelegate.scrollBottom();
                            }, 1000);
                        });
                    });

                });
            };

            $scope.getMessages();
            // var chat = firebase.database().ref("messages");
            // chat.on('value', function (data) {
            //     $timeout(function () {                    
            //         $timeout(function () {
            //             $ionicScrollDelegate.scrollBottom();
            //         }, 1000);
            //     });
            // }, function (errorObject) {
            //     //console.log("Error getting the properties: " + errorObject.code);
            // });
            $scope.sendMessage = function(type, message) {
                //console.log(message);
                var msgData = {};
                if (type == 'image') {
                    msgData = {
                        conversation: $stateParams.slug,
                        fromId: $rootScope.currentUser.uid,
                        toId: $scope.friend.uid,
                        updatedAt: new Date().getTime(),
                        image: message,
                        type: type
                    };
                } else {
                    msgData = {
                        conversation: $stateParams.slug,
                        fromId: $rootScope.currentUser.uid,
                        toId: $scope.friend.uid,
                        updatedAt: new Date().getTime(),
                        message: message,
                        type: type
                    };
                }
                var database = firebase.database();
                var newMsgKey = firebase.database().ref().child('messages').push().key;
                database.ref('messages/' + newMsgKey).set(msgData);
                var formatedMsg = "Nouveau message de (" + $scope.friend.name + ") : " + message;
                var promise = PushServices.sendPush($scope.friend.deviceId, formatedMsg, $location.path());
                promise.then(function(datas) {
                    var newMsgKey = firebase.database().ref().child('notifications').push().key;
                    var notifData = {
                        message: formatedMsg,
                        action: "message/" + $stateParams.slug,
                        fromUser: $rootScope.currentUser.uid,
                        user: $scope.friend.uid,
                        updatedAt: new Date().getTime(),
                        read: 0,
                        notificationKey: newMsgKey
                    };
                   
                    database.ref('notifications/' + newMsgKey).set(notifData);
                    firebase.database().ref('conversations/' + $stateParams.slug).update({
                        lastMessage: message.type == 'image' ? 'Une image.' : formatedMsg,
                        updatedAt: new Date().getTime()
                    });
                });
                $ionicScrollDelegate.scrollBottom();
                $scope.message = "";
                //Clear, and refresh to see the new messages.
                Utils.hide();
            };


            //Broadcast from our Utils.getPicture function that tells us that the image selected has been uploaded.
            $scope.$on('imageUploaded', function(event, args) {
                //Proceed with sending of image message.
                //console.log(args.imageUrl);
                $scope.sendMessage('image', args.imageUrl);
            });

            //Send picture message, ask if the image source is gallery or camera.
            $scope.sendPictureMessage = function() {
                var popup = Utils.confirm("ion-link", "{{ 'TAKEPHOTO_QUESTION' | translate }}", "ion-images", "ion-camera");
                popup.then(function(isCamera) {
                    var imageSource;
                    if (isCamera) {
                        imageSource = Camera.PictureSourceType.CAMERA;
                    } else {
                        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
                    }
                    //Show loading.
                    Utils.getPicture(imageSource);
                });
            };

            //Send text message.
            $scope.sendTextMessage = function() {
                if ($scope.message != '') {
                    $scope.sendMessage('text', $scope.message);
                }
            };

            //Enlarge selected image when selected on view.
            $scope.enlargeImage = function(url) {
                Utils.image(url);
            };

            //Scroll to bottom so new messages will be seen.
            $scope.scrollBottom = function() {
                $ionicScrollDelegate.scrollBottom(true);
            };

            //Scroll to top.
            $scope.scrollTop = function() {
                $ionicScrollDelegate.scrollTop(true);
            };

        }
    ]);
})();
