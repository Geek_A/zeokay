(function () {
    'use strict';
    zeokey.controller('SubscriptionCtrl', ['$scope', '$rootScope', '$location', '$translate', 'Utils', 'Popup',
        function ($scope, $rootScope, $location, $translate, Utils, Popup) {
            $scope.goTo = function (path) {
                $location.path(path);
                //console.log('SubscriptionCtrl');
            };


            $scope.unsubscribre = function () {
                var nbr_proof = $rootScope.currentProfile.subscription.remaining_proofs;

                var confirmPopup = Utils.confirmtxt("ion-information-circled",
                    $translate.instant('ALERT_UNSUBSCRIPTION', {
                        nbr_proof: nbr_proof
                    }),
                    "OK_VIDEO_1", "CANCEL");

                confirmPopup.then(function (res) {
                    if (res) {
                        firebase.database().ref('/stripe_customers/' + $rootScope.currentUser.uid + '/deletesubscription').push({
                            createdAt: new Date().getTime(),
                            formula: $rootScope.currentFormula.id
                        }).then(function () {
                            Utils.message(Popup.successIcon, "OPERATION_IN_PROGRESS");
                            $location.path("home");
                        });
                    }
                });
            }

        }
    ]);
})();