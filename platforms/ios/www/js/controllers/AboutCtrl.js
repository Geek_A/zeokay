(function() {
    'use strict';
    zeokey.controller('AboutCtrl', ['$scope', '$rootScope', '$location', '$ionicHistory',
        function($scope, $rootScope, $location, $ionicHistory) {
            $scope.goTo = function(path) {
                $location.path(path);
            };
            $scope.priviousUrl = "setting";
            if ($ionicHistory.backView()) {
                $scope.priviousUrl = $ionicHistory.backView().url;
            }

             $scope.close = function() {
                $scope.goTo('home');
            };
        }
    ]);
})();
