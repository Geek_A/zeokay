(function() {
    'use strict';
    zeokey.controller('DownloadCtrl', ['$scope', '$stateParams', '$rootScope', '$ionicPopup', '$location', '$ionicHistory', 'Utils', 'Popup',
        function($scope, $stateParams, $rootScope, $ionicPopup, $location, $ionicHistory, Utils, Popup) {

            $scope.emails = $rootScope.currentProfile ? [$rootScope.currentProfile.email] : [];
            $scope.isEmailValid = true;
           
            $scope.priviousUrl = "home";
            if ($ionicHistory.backView()) {
                $scope.priviousUrl = $ionicHistory.backView().url;
            }

            $scope.goTo = function(path) {
                $location.path(path);
            };

            $scope.clear = function(path) {
                $scope.isEmailValid = true;
            };

            /*call back method for chip*/
            $scope.render = function (email) {
                var reg = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;  
                email = email.toLowerCase().trim();              
                if ($scope.isEmailValid = reg.test(email)) {
                    $scope.emails.push(email);
                    $scope.$apply();
                    return email
                } else {
                    $scope.isEmailValid = false;
                    $scope.$apply();
                    return undefined;
                }
            };

            /*call back method for chip delete*/
            $scope.deleteChip = function(val) {
                return true;
            }

            $scope.doDownloadPaiement = function() {
                var user = $rootScope.currentUser;
                if (user) {
                    if (!$rootScope.sources || $rootScope.sources.length === 0) {
                        var confirmPopup = Utils.confirmtxt("ion-information-circled",
                            "POPUP_NO_CARD",
                            "SAVE", "CANCEL");
                        confirmPopup.then(function(res) {
                            if (res) {
                                $location.path("card");
                            }
                        });
                    } else {
                        Utils.show();
                        var videoId = $stateParams.slug;                      
                        firebase.database().ref('/stripe_customers/' + user.uid + '/download').push({
                            proof: videoId,
                            amount: $scope.amount * 100,
                            emails: $scope.emails.join(';'),
                            subscribedAt: new Date().getTime()
                        }).then(function() {
                            Utils.message(Popup.successIcon, 'PAYMENT_IN_PROGRESS');
                            $scope.goTo($scope.priviousUrl);
                        }).catch(function(error) {
                            Utils.message(Popup.errorIcon, error);
                        });                        
                    }                    
                }
            };


             //console.log($rootScope.selectedFormula);
            firebase.database().ref('/videos/' + $stateParams.slug).once('value').then(function(snapshot){
                var video = snapshot.val();
                $scope.amount = video.downloadedAt ? 0 : 3.99; 
            });
        }
    ]);
})();
